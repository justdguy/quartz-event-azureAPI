'use strict';

var mongoose = require('mongoose');

module.exports = function (Schema,mongoose) {
    var statusHistorySchema = new Schema({
        // adminUser : {
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref: 'userInfoModel'
        // },
        adminUser: String,
        adminUserId: String,
        adminName: String,
        status: {
            type: String,
            required: true
        },
        userId: String,
        // user:{
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref: 'userInfoModel'
        // },
        created: {
            type: Date,
            default: Date.now
        },
        updated: {
            type: Date,
            default: Date.now
        }
    });
    return mongoose.model('statushistory', statusHistorySchema);
}