'use strict';

module.exports = function (Schema,mongoose) {
    var signUpSchema = new Schema({
        invitationCode:String,
        userFullName:{  
           firstName:String,
           lastName:String
        },
        workEmail:String,
        company:{  
           companyName:String,
           companyWebSite:String
        },
        job:{  
           jobFunction:String,
           jobTitle:String
        },
        userPhoneNumber:{  
           phoneNumber:String,
           ext:String
        },
        previousWorkEmail:String,
        isAdmin: {
            type: Boolean,
            default: false
        },
        status: {
            type: String,
            defualt: 'approved'
        },
        created: {
            type: Date,
            default: Date.now
        },
        updated: {
            type: Date,
            default: Date.now
        }
    });
    return mongoose.model('admininfos', signUpSchema);
}