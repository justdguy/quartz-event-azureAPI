'use strict';

module.exports = function (Schema,mongoose) {
    var userInfoSchema = new Schema({
        invitationCode:{type:String,default: ""},
        userName: String,
        userFullName:{  
           firstName:String,
           lastName:String
        },
        workEmail:{ type: String, index: true },
        company:{  
           companyName:String,
           companyWebSite:String,
           companyLogo: {
               type: String,
               default: ""
           }
        },
        job:{  
           jobFunction:String,
           jobTitle:String
        },
        userPhoneNumber:{  
           phoneNumber:String,
           ext:String
        },
        notifications: {
            type: [String],
            default: []
        },
        previousWorkEmail:String,
        isAdmin: {
            type: Boolean,
            default: false
        },
        // createdBy:{

        // },
        status: {
            type: String,
            default: 'Applied'
        },
        activated:{
            type: Boolean,
            default: false
        },
        created: {
            type: Date,
            default: Date.now
        },
        updated: {
            type: Date,
            default: Date.now
        },
        lastLogin: {
            type: Date,
            default: null,
            required: false
        },
    });
    return mongoose.model('users', userInfoSchema);
}