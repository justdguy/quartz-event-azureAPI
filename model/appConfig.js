'use strict';

module.exports = function (Schema,mongoose) {
    var appConfigSchema = new Schema({
        jobFunctionList:Array,
        securityQuestions:{
            questions:Array
        },
        categories:{},
        bannedDomain:Array,
        industries: Array,
        numberOfEmployees: Array,
        companyRevenue: Array,
        hiddenFields: {
            primaryJobFunction: [String],
            areaOfInterest: [String]
        },
        termsAndConditions: String
    }, { strict: false });
    return mongoose.model('appconfigs', appConfigSchema);
}