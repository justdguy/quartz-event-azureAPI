'use strict';

var mongoose = require('mongoose');
// npm install sanitize-html
// var sanitizeHtml = require('sanitize-html');

module.exports = function (Schema,mongoose) {
    var emailsSchema = new Schema({
        userId: String,
        emailType: String,
        content: {
            type: String,
            required: true
        },
        userId: String,
        // user:{
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref: 'userInfoModel'
        // },
        created: {
            type: Date,
            default: Date.now
        },
        updated: {
            type: Date,
            default: Date.now
        }
    });
    return mongoose.model('emails', emailsSchema);
}