'use strict';

var mongoose = require('mongoose');

module.exports = function (Schema,mongoose) {
    var transferHistorySchema = new Schema({
        // adminUser : {
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref: 'userInfoModel'
        // },
        adminUserId: String,
        adminUser: String,
        oldEmail: {
            type: String,
            required: true
        },
        newEmail: {
            type: String,
            required: true
        },
        userId: String,
        // user:{
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref: 'userInfoModel'
        // },
        created: {
            type: Date,
            default: Date.now
        },
        updated: {
            type: Date,
            default: Date.now
        }
    });
    return mongoose.model('transferhistory', transferHistorySchema);
}