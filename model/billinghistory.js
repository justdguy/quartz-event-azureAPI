'use strict';

var mongoose = require('mongoose');

module.exports = function (Schema,mongoose) {
    var billingHistorySchema = new Schema({
        userId: String,
        // user:{
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref: 'userInfoModel',
        //     required: true
        // },
        description:{
            type: String,
            required: true
        },
        created: {
            type: Date,
            default: Date.now
        },
        updated: {
            type: Date,
            default: Date.now
        }
    });
    return mongoose.model('billinghistory', billingHistorySchema);
}