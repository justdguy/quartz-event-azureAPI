'use strict';
const bcrypt   = require('bcrypt-nodejs');
module.exports = function (Schema,mongoose) {
    var accActivationSchema = new Schema({
        workEmail:String,
        passWord:String,
        userId:String,
        userSecurityQuestion:{
           question:String,
           answer:String
        }
    });
    // methods ======================
    // generating a hash
    accActivationSchema.methods.generateHash = function(password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    };

    // checking if password is valid
    accActivationSchema.methods.validPassword = function(password) {
        return bcrypt.compareSync(password, this.local.password);
    };
    return mongoose.model('useraccountactivations', accActivationSchema);
}