'use strict';

module.exports = function (Schema,mongoose) {
    var companySchema = new Schema({
        userId:String,
        company:{
           industry:String,
           revenue:String,
           employeeSize:String
        }
    });
    return mongoose.model('usercompanyinfos', companySchema);
}