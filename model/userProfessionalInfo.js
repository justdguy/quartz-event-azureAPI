'use strict';

module.exports = function (Schema,mongoose) {
    var userProfessionalInfoSchema = new Schema({
        userId:String,
        currentWorkExperience:Object,
        previousWorkExperiances:Array
    }, { strict: false });
    return mongoose.model('userprofessionalinfos', userProfessionalInfoSchema);
}