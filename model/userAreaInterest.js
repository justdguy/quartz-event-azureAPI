'use strict';

module.exports = function (Schema,mongoose) {
    var areaIntrestSchema = new Schema({
        userId:String,
        mainAreaInterest: Object,
        otherAreaInterest: Object
    }, { strict: false });
    return mongoose.model('userareainterests', areaIntrestSchema);
}

/** moreJobFunctions
 * this more job function is an array of object 
 * which has name of job function and sub category
 */