'use strict';
var jwt = require('jsonwebtoken');
var request = require("request");
var cheerio = require('cheerio');
var fs = require('fs');
var path = require('path');
var azure = require('azure-storage');
var mongoose = require('mongoose');
var Gremlin = require('gremlin');
var MongoClient = require('mongodb').MongoClient;
// var url = "mongodb://devapp.infodat.com:27017/";
// var url = "mongodb://localhost:27017/";
const url ="mongodb://quartzb2bdocdb:rlsXjVMpC61mnFoNwkZddfJyy1Nwjlj13vz0Xgmd45DgaBmEElzSM6s0s2ZNdCV6FYjTEomScbaJWcod5VKorg==@quartzb2bdocdb.documents.azure.com:10255/quartz?ssl=true";
// const url = "mongodb://devapp.infodat.com:27017/";
const mongodbConn = "mongodb://quartzb2bdocdb.documents.azure.com:10255/quartz?ssl=true&replicaSet=globaldb";
// const mongodbConn = "mongodb://localhost:27017/quartz";
const mongodbClientConn = "mongodb://quartzb2bdocdb:rlsXjVMpC61mnFoNwkZddfJyy1Nwjlj13vz0Xgmd45DgaBmEElzSM6s0s2ZNdCV6FYjTEomScbaJWcod5VKorg==@quartzb2bdocdb.documents.azure.com:10255/?ssl=true";
const serverAddress = "https://quartzb2bweb-dev.azurewebsites.net/";
var databaseName = "quartz";

// var Client   = require('clearbit').Client;
// var clearbit = new Client({key: 'api_key'});
var clearbit = require('clearbit')('sk_820e9e7c42217bb47c74b2b7bfb823aa');
var ClearbitCompanySearch = clearbit.Company;

var nodeoutlook = require('nodejs-nodemailer-outlook');

// var sendgrid = require('sendgrid')('azure_5ba3d47cc5a11dc492e452cde0a63ef6@azure.com', 'QWsaZX21!@');

// -------------------- Code Structure --------------------- //
// For GET request
// ---------------
// Every get request parameters must be passed in the QUERY -- NOT -- IN PARAMS
// Some get request requires wrapper -- please continue reading to know how to use a wrapper

// For POST/PUT request
// ----------------
// Almost every post/put request requires wrapper
// Please continue reading to know how to use a wrapper

// Wrapper
// -------
// Wrapping a post request example
// --------------------------
// POST REQUEST BODY looks like this: 
// {
    // mainDataObject: {
        // field1 : "field1-Value"
        // field2 : "field2-Value"
    // },
    // wrapper: "mainDataObject", // the value must be the same with the mainDataObject field name
    // unwrapper: "mainDataObjectToReturn" // this is used to wrap the data to send back
// }

// Response(s)
// ------------
// Every response will be an object with fields: success, message, error, unwrapper
// Based on the request the unwrapper could be object or array


// new MongoClient(mongodbConn, {
//     auth: {
//           user: 'quartzb2bdocdb',
//           password: 'rlsXjVMpC61mnFoNwkZddfJyy1Nwjlj13vz0Xgmd45DgaBmEElzSM6s0s2ZNdCV6FYjTEomScbaJWcod5VKorg=='
//         }
// }).connect((err, db) => {
//     if (err) { console.log(err);
//     } else {
//     var dbo = db.db(databaseName);
//     dbo.collection("appconfigs").find({}).limit(10).toArray(function (err, result) {
//         if (!err) {
//             console.log(result);
//         } else {
//             console.log(err);
//         }
//         db.close();
//     });}
// });

var clientEmail = "admin@quartznetwork.com";
var clientPassword = "l219R%zF@06m";
var azureAccountName = "quartzblob";
var azureAccountKey = "mbHJVcEalsVuCwy9gtSiZzVFiP4y7zLKkAoKlNzP4hGRawFJGlh08NahfsnP9993AOaUdmJHEJpjMJ2UnousSw==";

var azureCosmoDBUser = "quartzb2bdocdb";
var azureCosmoDBPassword = "rlsXjVMpC61mnFoNwkZddfJyy1Nwjlj13vz0Xgmd45DgaBmEElzSM6s0s2ZNdCV6FYjTEomScbaJWcod5VKorg==";

var blobService = azure.createBlobService(azureAccountName, azureAccountKey);

module.exports = function (app, router, bodyParser) {

    function emailCampaign(){
        var CronJob = require('cron').CronJob;
        var job = new CronJob('00 30 11 * * 1-5', function() {
        /*
        * Runs every weekday (Monday through Friday)
        * at 11:30:00 AM. It does not run on Saturday
        * or Sunday.
        */
        }, function () {
            /* This function is executed when the job stops */
        },
        true, /* Start the job right now */
        timeZone /* Time zone of this job. */
        );
    }

    function sendAcceptanceMail(user){
        // var emailList = 'talk2ayodeji@gmail.com,mallika.madineni@infodatinc.com,' +  user.workEmail
        var token = jwt.sign(user, app.get('clientSecret'), {
            expiresIn: 60 * 60 * 24 // expires in 24 hours -60 * 60 * 24 
        });
        fs.readFile(path.resolve(__dirname, '../views/acceptance.html'), 'UTF-8', function(err, html){
            var content = html.replace("{{First Name}}", user.name);
            var $ = cheerio.load(content);
            $('#activateAccount').attr('href', serverAddress+ "?activateAccount="+token);
            nodeoutlook.sendEmail({
                auth: {
                    user: clientEmail,
                    pass: clientPassword
                }, from: clientEmail,
                to: user.workEmail,
                subject: 'Welcome to The Network, ' + user.name,
                html: $.html(),
                text: 'Acceptance Email'
            });
        })
    }

    function sendApplicationReceivedMail(user){
        // var emailList = 'talk2ayodeji@gmail.com,mallika.madineni@infodatinc.com,' +  user.workEmail
        var token = jwt.sign(user, app.get('clientSecret'), {
            expiresIn: 60 * 60 * 24 // expires in 24 hours -60 * 60 * 24 
        });
        fs.readFile(path.resolve(__dirname, '../views/success.html'), 'UTF-8', function(err, html){
            var content = html.replace("{{First Name}}", user.name);
            var $ = cheerio.load(content);
            nodeoutlook.sendEmail({
                auth: {
                    user: clientEmail,
                    pass: clientPassword
                }, from: clientEmail,
                to: user.workEmail,
                subject: 'Application Accepted',
                html: $.html(),
                text: 'Application Received'
            });
        })
    }
    function sendResetPasswordEmail(user){
        var response = false;
        var token = jwt.sign(user, app.get('clientSecret'), {
            expiresIn: 60 * 60// --> active for 1 hour // expires in 24 hours -> 60 * 60 * 24 
        });
        console.log(token);
        fs.readFile(path.resolve(__dirname, '../views/Reset_Password.html'), 'UTF-8', function(err, html){
            var content = html.replace("{{First Name}}", user.name);
            var $ = cheerio.load(content);
            $('#resetLink').attr('href', serverAddress+ "?changePassword="+token);
            // $('#resetLink').attr('href', token);
            nodeoutlook.sendEmail({
                auth: {
                    user: clientEmail,
                    pass: clientPassword
                }, from: clientEmail,
                to: user.workEmail,
                subject: 'Recovery email for ' + user.name,
                html: $.html(),
                text: 'Activate Your Account!'
            });
        })
        response = true;
        return response;
    }

    function sendDenialEmail(user){
        var response = false;
        var token = jwt.sign(user, app.get('clientSecret'), {
            expiresIn: 60 * 60// --> active for 1 hour // expires in 24 hours -> 60 * 60 * 24 
        });
        fs.readFile(path.resolve(__dirname, '../views/Reset_Password.html'), 'UTF-8', function(err, html){
            var content = html.replace("{{First Name}}", user.name);
            var $ = cheerio.load(content);
            nodeoutlook.sendEmail({
                auth: {
                    user: clientEmail,
                    pass: clientPassword
                }, from: clientEmail,
                to: user.workEmail,
                subject: 'Application Denied!',
                html: $.html(),
                text: 'Application Denied!'
            });
        })
        response = true;
        return response;
    }

    var getModelSchema = require('../model/mongoConnection');
    //now we should configure the API to use bodyParser and look for 
    //JSON data in the request body
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    //To prevent errors from Cross Origin Resource Sharing, we will set 
    //our headers to allow CORS with middleware like so:
    app.use(function (req, res, next) {
        /* res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Credentials', 'true');
        res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        //and remove cacheing so we get the most recent comments
        res.setHeader('Cache-Control', 'no-cache'); */
        // check header or url parameters or post parameters for token
        // var token = req.body.token || req.query.token || req.headers['x-access-token'];
        // // decode token
        // if (token) {
        //     // verifies secret and checks exp
        //     jwt.verify(token, app.get('clientSecret'), function(err, decoded) {
        //         if (err) {
        //             return res.json({
        //                 success: false,
        //                 message: 'Failed to authenticate token.'
        //             });
        //         } else {
        //             // if everything is good, save to request for use in other routes
        //             req.decoded = decoded;
        //             next();
        //         }
        //     });
        // } else {
        //     // if there is no token
        //     // return an error
        //     return res.status(403).send({
        //         success: false,
        //         message: 'No token provided.'
        //     });
        // }
        next();
    });
    mongoose.Promise = global.Promise;

    // const mongoUri = `mongodb://${process.env.COSMOSDB_ACCOUNT}:${process.env.COSMOSDB_KEY}@${
    //     process.env.COSMOSDB_ACCOUNT
    // }.documents.azure.com:${process.env.COSMOSDB_PORT}/${process.env.COSMOSDB_DB}?ssl=true`;

    //console.log("hahaahaha",process.env);
    // const mongodbConn = "mongodb://localhost/quartz";
    // mongoose.connect(url+databaseName, {
    //     useMongoClient: true
    // });
    mongoose.connect(mongodbConn, {
        auth: {
          user: azureCosmoDBUser,
          password: azureCosmoDBPassword
        }
    })

    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function () {
        console.log("connected to db");
    });

    /** adding the gremilin cosmos db code */
    // const client = Gremlin.createClient(
    //     443, 
    //     config.endpoint, 
    //     { 
    //         "session": false, 
    //         "ssl": true, 
    //         "user": `/dbs/${config.database}/colls/${config.collection}`,
    //         "password": config.primaryKey
    //     }
    // );

    /** example for inserting the data to graph */
    // function addVertex1(callback)
    // {
    //     console.log('Running Add Vertex1'); 
    //     client.execute("g.addV('person').property('id', 'thomas').property('firstName', 'Thomas').property('age', 44).property('userid', 1)", { }, (err, results) => {
    //         if (err) {
    //             return callback(console.error(err));
    //         }

    //         console.log("Result: %s\n", JSON.stringify(results));
    //         callback(null)
    //     });
    // }

    var signUpModel = getModelSchema(mongoose, 'signup');
    var appConfigModel = getModelSchema(mongoose, 'appconfig');
    var companyInfoModel = getModelSchema(mongoose, 'companyinfo');
    var userActivationModel = getModelSchema(mongoose, 'useractivation');
    var userAreaInterestModel = getModelSchema(mongoose, 'userareainterest');
    var userProfessionalInfoModel = getModelSchema(mongoose, 'userprofessionalinfo');
    var userInfoModel = getModelSchema(mongoose, 'users');
    var userProfilePictureModel = getModelSchema(mongoose, 'userprofilepicture');
    var userStatusHistoryModel = getModelSchema(mongoose, 'statushistory');
    var userBillingHistoryModel = getModelSchema(mongoose, 'billinghistory');
    var transferHistoryModel = getModelSchema(mongoose, 'transferhistory');
    var emailsModel = getModelSchema(mongoose, 'emails');

    router.get('/api', function (req, res) {
        res.send({ message: 'API Initialized!' });
    });
    
     router.post("/uploadProfileImage", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
        message: "Error: userId not found.",
        success: false,
        error: null
        }
        console.log(req.body)
        var reqBody = req.body;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        let reqWrapper = reqBody.wrapper;
        resp[responseUnwrapper] = null;
        if(reqBody[reqWrapper].userId){
            
            blobService.createContainerIfNotExists('taskcontainer', {
                publicAccessLevel: 'blob'
            }, function(error, result, response) {
                if (!error) {
                  // if result = true, container was created.
                  // if result = false, container already existed.
                  blobSvc.createBlockBlobFromLocalFile('quartzuser', reqBody[reqWrapper].userId, 'profileImage'+reqBody[reqWrapper].fileType, function(error, result, response){
                    if(!error){
                      // file uploaded
                      console.log(result);
                      res.json(result);
                    }
                  });
                }
            });
        }
    })

    router.get("/getHome", (req, res)=>{
        request({
            uri: "https://quartznetwork.com/",
        }, function(error, response, body) {
            res.send(response.body);
        });
    })
    // this is a test route - for serching the company list database provided by the client
    // post request: 
    // -- require [companyName] in the request body
    router.post("/testCompany", (req, res)=> {
        new MongoClient(mongodbConn, {
                auth: {
                    user: azureCosmoDBUser,
                    password: azureCosmoDBPassword
                }
            }).connect((err, db) => {
            if (err) throw err;
            var body = req.body;
            var dbo = db.db(databaseName);
            dbo.collection("companies").findOne({"companyName": { $regex: new RegExp("^" + body.companyName, "i") }, "companyWebsite": { $regex: new RegExp(body.companyWebsite, "i") } }, function(err, result) {
                if (err) throw err;
                res.json(result);
                db.close();
            });
        })
    });

    

    // this gets company information in the clearbit
    // get request
    // -- require [companyDomain] i the query
    router.get('/getCompanyInfoWithClearbit', function (req, res) {
         // resp is the object to send back to UI
         var resp = {
            message: "company domain (companyDomain) is not included in the search query",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        var search = reqQuery['companyDomain'];
        resp[responseUnwrapper] = null;
        if(search && search != ""){
            ClearbitCompanySearch.find({domain: search})
            .then(function (company) {
                resp[responseUnwrapper] = company;
                resp.success = true;
                resp.message = "Successful"
                res.status(200).json(resp);
            })
            .catch(ClearbitCompanySearch.QueuedError, function (err) {
                resp.error = err; // Company is queued
                res.json(resp);
            })
            .catch(ClearbitCompanySearch.NotFoundError, function (err) {
                res.json(err);
                console.log(err); // Company could not be found
                resp.error = err; // Company could not be found
                res.json(resp);
            })
            .catch(function (err) {
                resp.error = err; // Company could not be found
                resp.message = "Bad/invalid request, unauthorized, Clearbit error, or failed request";
                res.json(resp);
            });
        } else {
            res.json(resp);
        }
    });

    // authenticates a user
    // post request
    //  -- requires a wrapper
    router.post("/authenticateUser", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Error: either wrapper or email or password is not provided.",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].email && reqBody[reqWrapper].email != "" && reqBody[reqWrapper].passWord && reqBody[reqWrapper].passWord != "") {
            userInfoModel.findOne({ workEmail: reqBody[reqWrapper].email }).exec(function (errNoUser, user) {
                if (!errNoUser) {
                    if (user && user._id) {
                        userActivationModel.findOne({ userId: user._id, passWord: reqBody[reqWrapper].passWord }).exec(function (err, passWordMatch) {
                            if (!err) {
                                if (passWordMatch) {
                                    userInfoModel.update({_id: user._id}, { $set: {
                                        lastLogin: new Date()
                                    }}).exec(function(errUpdateingLastLogin, userLastLoginUpdated){
                                        if(!errUpdateingLastLogin){
                                            // console.log(userLastLoginUpdated);
                                        }
                                    })
                                    resp[responseUnwrapper] = user;
                                    resp.success = true;
                                    resp.message = "Successfully signed in.";
                                    res.status(200).json(resp);
                                } else {
                                    resp.success = true;
                                    resp.message = "Incorrect username/password. Please try again.";
                                    res.status(200).json(resp);
                                }
                            } else {
                                resp.success = false;
                                resp.message = "Incorrect username/password. Please try again.";
                                res.json(resp);
                            }
                        })
                    } else {
                        resp.success = true;
                        resp.message = "Either username or password entered is wrong";
                        res.status(200).json(resp);
                    }
                } else {
                    resp.success = true;
                    resp.message = "Error checking user.";
                    res.json(resp);
                }
            })
        } else {
            res.json(resp);
        }
    });

    // delete user User Activation
    router.delete("/deleteUser", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request has no userId",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        // let reqWrapper = reqQuery.wrapper;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqQuery["userId"]) {
            userInfoModel.remove({ _id: reqQuery["userId"] }, function (err) {
                if (!err) {
                    resp[responseUnwrapper] = true;
                    resp.success = true;
                    resp.message = "Deleted user successfully";
                    res.status(200).json(resp);
                } else {
                    resp.error = err
                    res.json(resp);
                }
            });
        } else {
            res.json(resp);
        }
    });

    router.get("/resetUserPassword", (req, res) => {
        var resp = {
            message: "Failed to send reset password email to user.",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        resp.responseUnwrapper = false;
        if(reqQuery.userId && reqQuery.userId != ""){
            userInfoModel.findOne({_id: reqQuery.userId}).exec(function(err, doc){
                if(!err){
                    var user = {};
                    user.workEmail = doc.workEmail;
                    user.userId = doc._id;
                    user.name = doc.userFullName.firstName + " " + doc.userFullName.lastName;
                    user.date = new Date();
                    var response = sendResetPasswordEmail(user);
                    if(response){
                        resp.responseUnwrapper = true;
                        resp.message = "Successful";
                        resp.success = true;
                        res.status(200).json(resp);
                    } else{
                        resp.responseUnwrapper = false;
                        resp.success = false;
                        resp.message = "Unsuccessful";
                        res.json(resp);
                    }
                } else {
                    resp.responseUnwrapper = false;
                    resp.message = "Unsuccessful";
                    res.json(resp);
                }
            })
        }
    });

    router.post("updateUserPassword", (req, res)=>{
        var resp = {
            message: "Request has no userId and/or newPassword",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "" && reqBody[reqWrapper].newPassword && reqBody[reqWrapper].newPassword != ""){
            userActivationModel.update({userId: reqBody[reqWrapper].userId}, {$set: { passWord: reqBody[reqWrapper].newPassword  }})
            .exec(function(errNoUser, found){
                if(!errNoUser){
                    if(found && found.nModified && found.nModified == 1){                      
                        userInfoModel.findOne({}).exec(function(errGettingUser, user){
                            if(!errGettingUser && user){
                                resp[responseUnwrapper] = user;
                                resp.message = "Successful";
                                resp.success = true;
                                res.status(200).json(resp);
                            }
                        })
                    } else{
                        resp[responseUnwrapper] = false;
                        resp.message = "Unsuccessful";
                        resp.success = false;
                        res.json(resp);
                    }
                } else {
                    resp[responseUnwrapper] = false;
                    resp.message = "Unsuccessful";
                    resp.success = false;
                    res.json(resp);
                }
            })
        } else {
            res.status(200).json(resp);
        }
    })

    router.post("/changeUserStatus", (req, res) => {
        var resp = {
            message: "Request has no userId and/or newStatus and/or adminUserId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].adminUserId && reqBody[reqWrapper].adminUserId != "" && reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "" && reqBody[reqWrapper].newStatus && reqBody[reqWrapper].newStatus != "") {
            userInfoModel.findOne({ _id: reqBody[reqWrapper].userId }).exec(function(err, user){
                user.status = reqBody[reqWrapper].newStatus;
                user.save(function(errChangingStatus, changedStatus){
                    if(!errChangingStatus){
                        if(changedStatus){
                            // save into status history
                            new userStatusHistoryModel({
                                status: reqBody[reqWrapper].newStatus,
                                // adminUser: reqBody[reqWrapper].adminUserId,
                                adminUser: reqBody[reqWrapper].adminUser,
                                adminUserId: reqBody[reqWrapper].adminUserId,
                                // user: reqBody[reqWrapper].userId,
                                userId: reqBody[reqWrapper].userId,
                                adminName: reqBody[reqWrapper].adminName
                            }).save(function(errSavingStatusHistory, savedStatusHistory){
                                if(!errSavingStatusHistory){
                                    resp[responseUnwrapper] = changedStatus
                                    resp.success = true;
                                    resp.message = "Successful";
                                    res.status(200).json(resp);                                    
                                } else{
                                    resp[responseUnwrapper] = null;
                                    resp.success = false;
                                    resp.message = "Unsuccessful";
                                    res.json(resp);
                                }
                            })
                        } else {
                            resp.message = "No user found";
                            res.json(resp);
                        }
                    } else {
                        resp.message = "Error changing user status";
                        res.json(resp);
                    }
                })
            })
        } else {
            res.json(resp);
        }
    })

    router.get("/getUserStatusHistory", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request query has no userId",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        if(reqQuery && reqQuery["userId"] && reqQuery["userId"] != ""){
            userStatusHistoryModel.find({userId: reqQuery["userId"]}).exec(function(err, userStatusHistory){
                if(!err){
                    resp[responseUnwrapper] = userStatusHistory;
                    resp.message = "Successful";
                    resp.success = true;
                    res.json(resp);
                } else {
                    resp.message = "Unsuccessful";
                    resp.success = false;
                    res.json(resp);
                }
            })
        } else {
            res.json(resp);
        }
    })

    router.post("/transferUserDetails", (req, res) => {
        var resp = {
            message: "Request has no userId and/or newStatus and/or adminUserId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if(reqBody[reqWrapper] && reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "" && reqBody[reqWrapper].adminUserId && reqBody[reqWrapper].adminUserId != "" && reqBody[reqWrapper].newEmail){
            userInfoModel.findOne({ _id: reqBody[reqWrapper].userId }).exec(function(errUser, user){
                if(!errUser){
                    var oldEmail = user.workEmail;
                    var newEmail = reqBody[reqWrapper].newEmail;
                    user.workEmail = reqBody[reqWrapper].newEmail;
                    user.save(function(errUpdatingUser, updatedUserData){
                        if(!errUpdatingUser){
                            new transferHistoryModel({
                                oldEmail: oldEmail,
                                newEmail: newEmail,
                                adminUser: reqBody[reqWrapper].adminUser,
                                // user: reqBody[reqWrapper].userId,
                                userId: reqBody[reqWrapper].userId,
                                adminUserId: reqBody[reqWrapper].adminUserId,
                                // adminUser: reqBody[reqWrapper].adminUserId
                            }).save(function(errTransfering, transfering){
                                if(!errTransfering){
                                    resp[responseUnwrapper] = true;
                                    resp.success = true;
                                    resp.message = "Successfull";
                                    res.status(200).json(resp);
                                } else {
                                    resp[responseUnwrapper] = false;
                                    resp.success = false;
                                    resp.message = "Unsuccessfull";
                                    res.json(resp);
                                }
                            })
                        } else {
                            resp[responseUnwrapper] = false;
                            resp.success = false;
                            resp.message = "Can't transfer user data";
                            res.json(resp);
                        }
                    })
                }
            })
        } else {
            res.json(resp);
        }
    })

    router.post("/newUserSignUp", function (req, res) {
        // resp is the object to send back to UI
        var resp = {
            message: "Unable to sign up",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper]) {
            new userInfoModel({
                invitationCode: reqBody[reqWrapper].invitationCode,
                userFullName: {
                    firstName: reqBody[reqWrapper].firstName,
                    lastName: reqBody[reqWrapper].lastName
                },
                userName: reqBody[reqWrapper].firstName,
                workEmail: reqBody[reqWrapper].workEmail,
                company: {
                    companyName: reqBody[reqWrapper].company,
                    companyWebSite: reqBody[reqWrapper].companySite

                },
                job: {
                    jobFunction: reqBody[reqWrapper].jobFunction,
                    jobTitle: reqBody[reqWrapper].jobTitle
                },
                userPhoneNumber: {
                    phoneNumber: reqBody[reqWrapper].phoneNumber,
                    ext: (reqBody[reqWrapper].extNumber) ? reqBody[reqWrapper].extNumber : ''
                },
                previousWorkEmail: reqBody[reqWrapper].oldEmail
                // could you stick to camel casing variable name "previousWorkEmail" instead of "PreviousWorkEmail"
                // userID instad of userId whenever needed
            }).save(function (err, doc) {
                //console.log(doc)
                if (err) {
                    resp.error = err;
                    res.json(resp)
                } else {
                    resp[responseUnwrapper] = doc;
                    resp.success = true;
                    resp.message = "New User Signed Up";
                    res.status(200).json(resp);
                    ClearbitCompanySearch.find({domain: reqBody[reqWrapper].companySite})
                    .then(function (company) {
                        var userCompanyWebsite = reqBody[reqWrapper].companySite.toLowerCase();
                        var userCompanyName = reqBody[reqWrapper].company.toLowerCase();
                        var companyName = company.name.toLowerCase();
                        var companyDomain = company.domain.toLowerCase();
                        if(company && companyName && companyName.trim() != "" && companyDomain && companyDomain.trim() != ""){
                            if(userCompanyWebsite.toLowerCase() == companyDomain.toLowerCase() && userCompanyName.toLowerCase() == companyName.toLowerCase()){
                                doc.company.companyLogo = company.logo;
                                doc.save(function(errUpdataUserCompanyLogo, updataUserCompanyLogo){
                                    if(!errUpdataUserCompanyLogo){
                                        // company logo is saved
                                        console.log(updataUserCompanyLogo);
                                    }
                                });
                            } 
                        }
                    })
                    .catch(ClearbitCompanySearch.QueuedError, function (err) {
                        console.log("Error saving company logo");
                    })
                    .catch(ClearbitCompanySearch.NotFoundError, function (err) {
                        console.log("Error saving company logo");
                    })
                    .catch(function (err) {
                        console.log("Error saving company logo");
                    });
                    // userCompany saved;
                    var user = {};
                    user.workEmail = doc.workEmail;
                    user.userId = doc._id;
                    user.name = doc.userFullName.firstName + " " + doc.userFullName.lastName;
                    console.log(user)
                    sendAcceptanceMail(user);
                    new MongoClient(mongodbConn, {
                        auth: {
                            user: azureCosmoDBUser,
                            password: azureCosmoDBPassword
                        }
                    }).connect((err, db) => {
                        if (err) throw err;
                        var dbo = db.db(databaseName);
                        dbo.collection("companies").findOne({
                            "companyName": { $regex: new RegExp(reqBody[reqWrapper].company.toLowerCase(), "i") }, 
                            "companyWebsite": { $regex: new RegExp(reqBody[reqWrapper].companySite.toLowerCase(), "i") } },
                            function (err, result) {
                            if (!err) {
                                if(result){
                                    new companyInfoModel({
                                        userId: doc._id,
                                        company: {
                                            industry: (result.companyPrimaryIndustry) ? result.companyPrimaryIndustry : "",
                                            // revenue: (result.companyRevenue) ? result.companyRevenue : "",
                                            // employeeSize: (result.numberOfEmployees) ? result.numberOfEmployees : ""
                                            revenue: (result.companyRevenueRange) ? result.companyRevenueRange : "",
                                            employeeSize: (result.employeeRange) ? result.employeeRange : ""
                                        }
                                    }).save(function(errSavingUserCompanyInfo, savingUserCompanyInfo){
                                        if(!errSavingUserCompanyInfo){
                                           
                                            
                                            // console.log(savingUserCompanyInfo);
                                        }
                                    })
                                }
                            } else {
                                // "Error saving user company info"
                            }
                            db.close();
                        });
                    })
                }
            });
        } else {
            res.json(resp);
        }

    });

    router.post("/saveAdminUser", function (req, res) {
        // resp is the object to send back to UI
        var resp = {
            message: "Unable to sign up",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper]) {
            if(reqBody[reqWrapper].userId){
                // update admin user 
                userInfoModel.findOne({ _id: reqBody[reqWrapper].userId}).exec(function(err, docs){
                    if(!err){
                        docs.workEmail = (reqBody[reqWrapper].workEmail) ? reqBody[reqWrapper].workEmail : docs.workEmail;
                        docs.notifications = (reqBody[reqWrapper].notifications) ? reqBody[reqWrapper].notifications : docs.notifications;
                        docs.userFullName.firstName = (reqBody[reqWrapper].firstName) ? reqBody[reqWrapper].firstName : docs.userFullName.firstName;
                        docs.userFullName.lastName = (reqBody[reqWrapper].lastName) ? reqBody[reqWrapper].lastName : docs.userFullName.lastName;
                        docs.isAdmin = (reqBody[reqWrapper].isAdmin) ? reqBody[reqWrapper].isAdmin : docs.isAdmin;
                        docs.save(function(errUpdatingUser, updatedUser){
                            if(!errUpdatingUser){
                                resp[responseUnwrapper] = updatedUser;
                                resp.success = true;
                                resp.message = "Successfull";
                                res.status(200).json(resp);
                            } else {
                                resp[responseUnwrapper] = null;
                                resp.message = "Error updating user details";
                                res.json(resp);
                            }
                        })
                    }
                })
            } else {
                new userInfoModel({
                    invitationCode: "",
                    userFullName: {
                        firstName: reqBody[reqWrapper].firstName,
                        lastName: reqBody[reqWrapper].lastName
                    },
                    workEmail: reqBody[reqWrapper].workEmail,
                    company: {
                        companyName: "",
                        companyWebSite: "",
                        compaanyLogo: ""
                    },
                    notifications: (reqBody[reqWrapper].notifications) ? reqBody[reqWrapper].notifications : [],
                    job: {
                        jobFunction: "",
                        jobTitle: ""
                    },
                    isAdmin: true,
                    // isAdmin: (reqBody[reqWrapper].isAdmin) ? reqBody[reqWrapper].isAdmin : false,
                    userPhoneNumber: {
                        phoneNumber: (reqBody[reqWrapper].phoneNumber) ? reqBody[reqWrapper].phoneNumber : "",
                        ext: (reqBody[reqWrapper].extNumber) ? reqBody[reqWrapper].extNumber : ""
                    },
                    status: "admin",
                    previousWorkEmail: ""
                    // could you stick to camel casing variable name "previousWorkEmail" instead of "PreviousWorkEmail"
                    // userID instad of userId whenever needed
                }).save(function (err, doc) {
                    //console.log(doc)
                    if (!err) {
                        if(doc._id){
                            new userActivationModel({
                                passWord: (typeof reqBody[reqWrapper].passWord !== undefined) ? reqBody[reqWrapper].passWord : "",
                                userId: (typeof doc._id !== undefined && doc._id) ? doc._id : "",
                                userSecurityQuestion: {
                                    question: (typeof reqBody[reqWrapper].secQuestion !== undefined) ? reqBody[reqWrapper].secQuestion : "",
                                    answer: (typeof reqBody[reqWrapper].secAnswer !== undefined) ? reqBody[reqWrapper].secAnswer : ""
                                }
                            }).save(function(errCreatingNewUser, newUser){
                                if(!errCreatingNewUser){
                                    resp[responseUnwrapper] = doc;
                                    resp.success = true;
                                    resp.message = "Successful";
                                    res.status(200).json(resp);
                                } else {
                                    resp[responseUnwrapper] = null;
                                    resp.message ="Error creating new user";
                                    res.json(resp);
                                }
                            });
                        } else {
                            resp[responseUnwrapper] = null;
                            resp.message = "Error saving user details";
                            res.json(resp);
                        }
                    } else {
                        resp[responseUnwrapper] = null;
                        resp.message = "Error saving user details";
                        res.json(resp);
                    }
                });
            }
        } else {
            resp.message = "Could not find wrapper in the request body";
            resp.error = true;
            res.json(resp);
        } 
    });

    router.get('/confirmActivationCode', (req, res)=>{
        // resp is the object to send back to UI
        var resp = {
            message: "Failed to authenticate token.",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        if(reqQuery && reqQuery.token){
        jwt.verify(reqQuery.token, app.get('clientSecret'), function(err, decoded) {
                if (err) {
                    res.json(resp);
                } else {
                    userInfoModel.findOne({_id: decoded.userId}).exec(function(err, user){
                        if(!err){
                            console.log(decoded)
                            // if everything is good, save to request for use in other routes
                            resp[responseUnwrapper] = user;
                            resp.success = true;
                            resp.message = "successful";
                            res.status(200).json(resp);
                        }
                    })
                }
            });
        }
    })

    router.get('/confirmResetCode', (req, res)=>{
        // resp is the object to send back to UI
        var resp = {
            message: "Failed to authenticate token.",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        resp.responseUnwrapper = null;
        if(reqQuery && reqQuery.token){
        jwt.verify(reqQuery.token, app.get('clientSecret'), function(err, decoded) {
                if (err) {
                    resp.error = err;
                    res.json(resp);
                } else {
                    resp.success = true;
                    // userInfoModel.findOne({_id: decoded.userId}).exec(function(err, user){
                    //     if(!err){
                    //         console.log(decoded)
                    //         // if everything is good, save to request for use in other routes
                    //         resp[responseUnwrapper] = user;
                    //         resp.success = true;
                    //         resp.message = "successful";
                    //         res.status(200).json(resp);
                    //     }
                    // })
                }
            });
        }
    })

    router.get("/getCompanyDetail", (req, res) => {

        // resp is the object to send back to UI
        var resp = {
            message: "Unable to fetch app config",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        var search = reqQuery['search'];
        if (search) {
            new MongoClient(mongodbConn, {
                        auth: {
                            user: azureCosmoDBUser,
                            password: azureCosmoDBPassword
                        }
                    }).connect((err, db) => {
                if (err) throw err;
                var searchText = (reqQuery['search']) ? reqQuery['search'] : '';
                var dbo = db.db(databaseName);
                dbo.collection("usercompanyinfos").find({ "companyName": { $regex: new RegExp("^" + searchText.toLowerCase(), "i") } }).limit(10).toArray(function (err, result) {
                    if (!err) {
                        resp.success = true;
                        resp.message = "Succesful";
                        resp[responseUnwrapper] = result;
                        res.status(200).json(resp)
                    } else {
                        resp.error = err;
                        resp.message = "Error retrieving app config object"
                        resp.json(resp);
                    }
                    db.close();
                });
            });
        } else {
            resp.message = "Request has no body";
            res.json(resp);
        }

    })

    // get app config
    router.get("/getAppConfig", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Unable to fetch app config",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        // resp[responseUnwrapper] = [];
        appConfigModel.find({}).exec(function (err, appConfigDocs) {
            if (!err) {
                var appConfigDoc = (typeof appConfigDocs[0] !== undefined) ? appConfigDocs[0] : null;
                if (appConfigDoc == null) {
                    resp.success = false;
                    resp.message = "You do not have any app config document";
                } else {
                    resp.success = true;
                    resp.message = "Succesful";
                }
                resp[responseUnwrapper] = appConfigDoc;
                res.status(200).json(resp)
            } else {
                resp.error = err;
                resp.message = "Error retrieving app config object"
                resp.json(resp);
            }
        })
    });

    router.put("/updateUserDatails", (req, res)=>{
        var resp = {
            message: "Request has no body or userId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if(reqBody && reqBody[reqWrapper].userId) {
            // userInfoModel.update({_id: reqBody[reqWrapper].userId}, {
            //     $set: {
            //         "userFullName.firstName": reqBody[reqWrapper].firstName,
            //         "userFullName.lastName": reqBody[reqWrapper].lastName,
            //         "workEmail": reqBody[reqWrapper].workEmail,
            //         "userPhoneNumber.phoneNumber": reqBody[reqWrapper].phoneNumber,
            //         // "userPhoneNumber.ext": reqBody[reqWrapper].extNumber,
            //         "company.companyName": reqBody[reqWrapper].companyName,
            //         "job.jobFunction": reqBody[reqWrapper].jobFunction,
            //     }
            // })
            userInfoModel.findOne({_id: reqBody[reqWrapper].userId}).exec(function(errUser, user){
                if(!errUser){
                    if(user){
                        user.userFullName.firstName = (reqBody[reqWrapper].firstName) ? reqBody[reqWrapper].firstName : user.userFullName.firstName;
                        user.userFullName.lastName = (reqBody[reqWrapper].lastName) ? reqBody[reqWrapper].lastName : user.userFullName.lastName;
                        user.workEmail= (reqBody[reqWrapper].workEmail) ? reqBody[reqWrapper].workEmail :user.workEmail;
                        user.userPhoneNumber.phoneNumber = (reqBody[reqWrapper].phoneNumber ) ? reqBody[reqWrapper].phoneNumber : user.userPhoneNumber.phoneNumber;
                        user.userPhoneNumber.ext = (reqBody[reqWrapper].extNumber ) ? reqBody[reqWrapper].extNumber : user.userPhoneNumber.ext;
                        user.company.companyName = (reqBody[reqWrapper].companyName) ? reqBody[reqWrapper].companyName :user.company.companyName;
                        user.job.jobFunction = (reqBody[reqWrapper].jobFunction) ? reqBody[reqWrapper].jobFunction : user.job.jobFunction;
                        user.job.jobTitle = (reqBody[reqWrapper].jobTitle) ? reqBody[reqWrapper].jobTitle : user.job.jobTitle;
                        user.save(function(errUpdatingUser, updatedUser){
                            if(!errUpdatingUser){
                                resp[responseUnwrapper] = true;
                                resp.success = true;
                                resp.message = "Successful";
                                res.status(200).json(resp);
                            } else {
                                resp.success = false;
                                resp.message = "Unsuccessful";
                                res.json(resp);
                            }
                        })
                    } else {
                        resp.success = false;
                        resp.message = "No user found";
                        res.json(resp);
                    }
                } else{
                    resp[responseUnwrapper] = null;
                    resp.success = false;
                    resp.error = errUser;
                    resp.message = "Unsuccessful";
                    res.json(resp);
                }
            })
        } else {
            res.json(resp);
        }
    });

    // update app config object 
    router.put("/updateAppConfig", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "",
            success: false,
            error: null
        }
        // console.log(req.body);
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody !== undefined && reqBody && reqBody[reqWrapper]) {
            appConfigModel.find({}).exec(function (errGettingAppConfig, appConfigDocs) {
                if (!errGettingAppConfig) {
                    // the appConfigDocs should have only one document else empty doc
                    // get the appConfigDoc
                    var appConfigDoc = (typeof appConfigDocs[0] !== undefined) ? appConfigDocs[0] : null;
                    if (appConfigDoc == null) {
                        resp.success = false;
                        resp.message = "You do not have app config document";
                        res.json(resp);
                        return;
                    }
                    // appConfigDoc = reqBody[reqWrapper].appConfig;
                    // console.log(appConfigDoc);
                    // appConfigDoc.save(function(errUpdating, updatedAppConfig){
                    //     if (!errUpdating) {
                    //         resp.success = true;
                    //         resp.message = "Appconfig updated successfully.";
                    //         resp[responseUnwrapper] = updateDoc;
                    //         res.status(200).json(resp);
                    //     } else {
                    //         resp.message = "Error updating Appconfig.";
                    //         resp.error = errUpdating;
                    //         res.json(resp);
                    //     }
                    // })
                    // console.log(appConfigDoc);
                    // do necessary update
                    appConfigModel.update({ _id: appConfigDoc._id }, {
                        $set: {
                            jobFunctionList: (reqBody[reqWrapper].jobFunctionList) ? reqBody[reqWrapper].jobFunctionList : appConfigDoc.jobFunctionList,
                            securityQuestions: (reqBody[reqWrapper].securityQuestions) ? reqBody[reqWrapper].securityQuestions : appConfigDoc.securityQuestions,
                            bannedDomain: (reqBody[reqWrapper].bannedDomain) ? reqBody[reqWrapper].bannedDomain : appConfigDoc.bannedDomain,
                            industries: (reqBody[reqWrapper].industries) ? reqBody[reqWrapper].industries : appConfigDoc.industries,
                            numberOfEmployees: (reqBody[reqWrapper].numberOfEmployees) ? reqBody[reqWrapper].numberOfEmployees : appConfigDoc.numberOfEmployees,
                            companyRevenue: (reqBody[reqWrapper].companyRevenue) ? reqBody[reqWrapper].companyRevenue : appConfigDoc.companyRevenue,
                            categories: (reqBody[reqWrapper].categories) ? reqBody[reqWrapper].categories : appConfigDoc.categories,
                            hiddenFields: (reqBody[reqWrapper].hiddenField) ? reqBody[reqWrapper].hiddenField : appConfigDoc.hiddenField,
                            termsAndConditions : (reqBody[reqWrapper].termsAndConditions) ? reqBody[reqWrapper].termsAndConditions : appConfigDoc.termsAndConditions,
                            faqs : (reqBody[reqWrapper].faqs) ? reqBody[reqWrapper].faqs : appConfigDoc.termsAndConditions,
                            policy : (reqBody[reqWrapper].policy) ? reqBody[reqWrapper].policy : appConfigDoc.policy
                        }
                    }).exec(function (errUpdating, updateDoc) {
                        if (!errUpdating) {
                            appConfigModel.find({}).exec(function (err, appConfigDocs) {
                                if (!err) {
                                    var appConfigDoc = (typeof appConfigDocs[0] !== undefined) ? appConfigDocs[0] : null;
                                    if (appConfigDoc == null) {
                                        resp.success = false;
                                        resp.message = "You do not have any app config document";
                                    } else {
                                        resp.success = true;
                                        resp.message = "Succesful";
                                    }
                                    resp[responseUnwrapper] = appConfigDoc;
                                    res.status(200).json(resp)
                                } else {
                                    resp.error = err;
                                    resp.message = "Error retrieving app config object"
                                    resp.json(resp);
                                }
                            })
                            // resp.success = true;
                            // resp.message = "Appconfig updated successfully.";
                            // resp[responseUnwrapper] = updateDoc;
                            // res.status(200).json(resp);
                        } else {
                            resp.message = "Error updating Appconfig.";
                            resp.error = errUpdating;
                            res.json(resp);
                        }
                    });
                } else {
                    resp.message = "Failed getting app config document";
                    resp.error = errGettingAppConfig;
                    res.json(resp);
                }
            })
        } else {
            resp.message = "Request has no body or userId";
            res.json(resp);
        }
    });

    // get all users
    router.get("/getUserInfo", (req, res) => {
        var resp = {
            message: "Unable to fetch user info.",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        var userId = reqQuery["userId"];
        if (reqQuery && reqQuery && userId != "") {
            userInfoModel.findOne({ _id: userId }).exec(function (err, userDoc) {
                if (!err) {
                    if (userDoc == null) {
                        resp.success = false;
                        resp.message = "There is no matched document";
                    } else {
                        resp.success = true;
                        resp.message = "Succesful";
                    }
                    resp[responseUnwrapper] = userDoc;
                    res.status(200).json(resp)
                } else {
                    resp.error = err;
                    resp.message = "Error retrieving user info document";
                    res.json(resp)
                }
            })
        } else {
            resp.message = "Request has no query or userId";
            res.json(resp);
        }
    });

    router.post("/filterUserList", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request has no body or userId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = (reqBody.wrapper) ? reqBody.wrapper: filter;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = [];
        if(reqBody[reqWrapper]){
            var searchQuery = null;
            var jobFunctionList = [];
            var status = [];
            var name = (reqBody[reqWrapper].name) ? reqBody[reqWrapper].name : null;
            var receivedStatus = (reqBody[reqWrapper].status) ? reqBody[reqWrapper].status : [];
            var receivedJobFunctions = (reqBody[reqWrapper].jobFunctionList) ? reqBody[reqWrapper].jobFunctionList : [];
            
            var query = [];
            query.push({isAdmin: false});
            if(name){
                query.push({ $or : [ { "company.companyName" : { $regex: new RegExp(name, "i") } }, { "userFullName.firstName" : { $regex: new RegExp(name, "i") }  }, { "userFullName.lastName" : { $regex: new RegExp(name, "i") } } ] })
            }
            if(receivedStatus && receivedStatus.length > 0){
                var filterStatus = [];
                for(var i = 0; i < receivedStatus.length; i++){
                    // filterStatus.push({"status": { $regex: new RegExp(receivedStatus[i], "i") }});
                    filterStatus.push({"status": { $regex: receivedStatus[i]}});
                }
                query.push({ $or :  filterStatus});
            }
            if(receivedJobFunctions && receivedJobFunctions.length > 0){
                var filterJobFunctions = [];
                for(var i = 0; i < receivedJobFunctions.length; i++){
                    filterJobFunctions.push({"job.jobFunction": { $regex: new RegExp(receivedJobFunctions[i], "i") }});
                    // filterJobFunctions.push({"job.jobFunction": { $regex: new RegExp(receivedJobFunctions[i], "i") }});
                }
                query.push({ $or : filterJobFunctions});
            }
            searchQuery = userInfoModel.find({$and : query})
            if(reqBody[reqWrapper].sort && reqBody[reqWrapper].sort.sortField != "" && reqBody[reqWrapper].sort.sortType != ""){
                console.log(reqBody[reqWrapper].sort)
                var sortField = reqBody[reqWrapper].sort.sortField;
                var sortType = reqBody[reqWrapper].sort.sortType;
                var sorting = {};
                sorting[sortField] = sortType;
                searchQuery.sort(sorting);
            }
            searchQuery.exec(function(errUserList, result){
                if(!errUserList){
                    if(result && result instanceof Array && result.length >0){
                        resp[responseUnwrapper] = result;
                    } else {
                        resp[responseUnwrapper] = [];
                    }
                    resp.success = true;
                    resp.message = "Successul";
                    res.status(200).json(resp);
                } else {
                    resp.message = "Error fetching data";
                    res.json(resp);
                }
            })
        } else {
            res.json(resp);
        }
    })

    router.put("/updateUserPersonalInfo", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request has no body or userId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;      
        if (reqBody[reqWrapper] && reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "") {
            userInfoModel.findOne({ _id: reqBody[reqWrapper].userId }).exec(function (err, userInfo) {
                if (!err) {
                    if (userInfo != null) {
                        userInfo.userPhoneNumber = {
                            phoneNumber: (reqBody[reqWrapper].phoneNumber) ? reqBody[reqWrapper].phoneNumber : userInfo.userPhoneNumber.phoneNumber,
                            ext: (reqBody[reqWrapper].extNumber) ? reqBody[reqWrapper].extNumber : userInfo.userPhoneNumber.extNumber
                        }
                        userInfo.company.companyName = (reqBody[reqWrapper].companyName) ? reqBody[reqWrapper].companyName : userInfo.company.companyName;
                        var currentExp = {};
                        currentExp.bio = reqBody[reqWrapper].bio;
                        currentExp.title = reqBody[reqWrapper].jobFunction;
                        currentExp.currentExp = (reqBody[reqWrapper].currentExp) ? reqBody[reqWrapper].currentExp : "";
                        // update data in user collection
                        userInfo.save(function (errUpdatingUser, updatedUser) {
                            if (!errUpdatingUser) {
                                userProfessionalInfoModel.findOne({
                                    userId: reqBody[reqWrapper].userId
                                }).exec(function (errGettingUserProfInfo, userProfInfo) {
                                    if (!errGettingUserProfInfo) {
                                        if(userProfInfo){
                                            // update user current work experience if found
                                            var previousWorkExperiances = (userProfInfo["previousWorkExperiances"]) ? userProfInfo["previousWorkExperiances"] : [];
                                            var currentWorkExperience = userProfInfo["currentWorkExperience"];
                                            userProfInfo.currentWorkExperience = currentExp;
                                            userProfInfo.previousWorkExperiances = previousWorkExperiances;
                                            userProfInfo.save(function(errUpdateCurrExp, updatedWorkExp){
                                                if(!errUpdateCurrExp){
                                                    var dataToSend = {
                                                        title: updatedUser.job.jobTitle,
                                                        phoneNumber: updatedUser.userPhoneNumber.phoneNumber,
                                                        ext: updatedUser.userPhoneNumber.ext,
                                                        bio: (userProfInfo && userProfInfo.currentWorkExperience && userProfInfo.currentWorkExperience.bio) ? userProfInfo.currentWorkExperience.bio : "",
                                                        currentExp: (updatedWorkExp && updatedWorkExp.currentWorkExperience && updatedWorkExp.currentWorkExperience.currentExp) ? updatedWorkExp.currentWorkExperience.currentExp : "",
                                                        jobFunction: updatedUser.job.jobFunction
                                                    }
                                                    resp[responseUnwrapper] = dataToSend;
                                                    resp.success = true;
                                                    resp.message = "Successful";
                                                    // console.log(succesUpdating)
                                                    res.status(200).json(resp);
                                                } else {
                                                    resp.success = false;
                                                    resp.message = "Unsuccessful updating user current work experience";
                                                    res.json(resp);
                                                }
                                            })
                                        } else {
                                            // create new work experience if not found 
                                            new userProfessionalInfoModel({
                                                userId: reqBody[reqWrapper].userId,
                                                currentWorkExperience: currentExp,
                                                previousWorkExperiances: []
                                            }).save(function(errSavingNewExp, createdNewExp){
                                                if(!errSavingNewExp){
                                                    var dataToSend = {
                                                        title: updatedUser.job.jobTitle,
                                                        phoneNumber: updatedUser.userPhoneNumber.phoneNumber,
                                                        ext: updatedUser.userPhoneNumber.ext,
                                                        bio: (createdNewExp && createdNewExp.currentWorkExperience && createdNewExp.currentWorkExperience.bio) ? createdNewExp.currentWorkExperience.bio : "",
                                                        currentExp: (createdNewExp && createdNewExp.currentWorkExperience && createdNewExp.currentWorkExperience.currentExp) ? createdNewExp.currentWorkExperience.currentExp : "",
                                                        jobFunction: updatedUser.job.jobFunction
                                                    }
                                                    resp[responseUnwrapper] = dataToSend;
                                                    resp.success = true;
                                                    resp.message = "Successful";
                                                    res.json(resp);
                                                } else {
                                                    resp.success = false;
                                                    resp.message = "Unsuccessful creating user current experience";
                                                    res.json(resp);
                                                }
                                            });
                                        }
                                        // userProfessionalInfoModel.update(
                                        // {
                                        //     userId: reqBody[reqWrapper].userId
                                        // }, { $set: { 
                                        //     "currentWorkExperience.bio": reqBody[reqWrapper].bio
                                        // }}).exec(function(errUpdating, succesUpdating){
                                        //     if(!errUpdating){
                                        //         var dataToSend = {
                                        //             phoneNumber: updatedUser.userPhoneNumber.phoneNumber,
                                        //             ext: updatedUser.userPhoneNumber.ext,
                                        //             bio: (reqBody[reqWrapper].bio) ? reqBody[reqWrapper].bio : "",
                                        //             jobFunction: updatedUser.job.jobFunction
                                        //         }
                                        //         resp[responseUnwrapper] = dataToSend;
                                        //         resp.success = true;
                                        //         resp.message = "Successful";
                                        //         res.json(resp);
                                        //     }
                                        // });
                                    } else {
                                        resp.message = "Could not find any matched User Info";
                                        res.json(resp);
                                    }
                                });
                            } else {
                                resp.error = errSavingComp;
                                res.json(resp);
                            }
                        })
                    } else {
                        resp.message = "Could not find any matched company";
                        res.json(resp);
                    }
                } else {
                    resp.message = "Error updating company";
                    res.json(resp);
                }
            });
        } else {
            res.json(resp);
        }
    });

    // get user Info
    router.get("/getUsers", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Unable to sign up",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        // resp[responseUnwrapper] = [];
        if (reqQuery) {
            userInfoModel.find({isAdmin: false}).limit(300).exec(function (err, userDoc) {
                if (!err) {
                    if (userDoc == null) {
                        resp.message = "There is no matched document";
                    } else {
                        resp.message = "Succesful";
                    }
                    resp.success = true;
                    resp[responseUnwrapper] = userDoc;
                    res.status(200).json(resp)
                } else {
                    resp.error = err;
                    resp.message = "Error retrieving user info document";
                    res.json(resp)
                }
            })
        } else {
            resp.message = "Request has no query or userId";
            res.json(resp);
        }
    });

    // get admin users Info
    router.get("/getAdminUsers", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Unable to sign up",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        // resp[responseUnwrapper] = [];
        if (reqQuery) {
            userInfoModel.find({isAdmin: true}).limit(300).exec(function (err, userDoc) {
                if (!err) {
                    if (userDoc == null) {
                        resp.message = "There is no matched document";
                    } else {
                        resp.message = "Succesful";
                    }
                    resp.success = true;
                    resp[responseUnwrapper] = userDoc;
                    res.status(200).json(resp)
                } else {
                    resp.error = err;
                    resp.message = "Error retrieving user info document";
                    res.json(resp)
                }
            })
        } else {
            resp.message = "Request has no query or userId";
            res.json(resp);
        }
    });

    // get companyInfo
    router.get("/getCompanyInfo", (req, res) => {
        var resp = {
            message: "Unable to fetch company info.",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        var userId = reqQuery["userId"];
        if (reqQuery !== undefined && reqQuery && userId != "") {
            companyInfoModel.findOne({ userId: userId }).exec(function (err, companyDoc) {
                if (!err) {
                    var companyDocTosend = {}
                    if (companyDoc == null) {
                        resp.success = false;
                        resp.message = "There is no matched document";
                        res.json(resp)
                    } else {
                        companyDocTosend._id = companyDoc._id;
                        companyDocTosend.userId = companyDoc.userId;
                        companyDocTosend.industry = companyDoc.company.industry;
                        companyDocTosend.revenue = companyDoc.company.revenue;
                        companyDocTosend.employeeSize = companyDoc.company.employeeSize;
                        resp.success = true;
                        resp.message = "Succesful";
                        resp[responseUnwrapper] = (Object.keys(companyDocTosend).length > 0) ? companyDocTosend : companyDoc.company;
                        res.status(200).json(resp)
                    }
                } else {
                    resp.error = err;
                    resp.message = "Error retrieving company info document";
                    res.json(resp)
                }
            })
        } else {
            resp.message = "Request query has no userId";
            res.json(resp);
        }
    });

    // save companyInfo
    // I supposed to verify userId before saving it but can't do that now
    router.post("/saveCompanyInfo", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request has no body or userId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "") {
            if (reqBody[reqWrapper]._id && reqBody[reqWrapper]._id != "") {
                companyInfoModel.findOne({
                    userId: reqBody[reqWrapper].userId,
                    _id: reqBody[reqWrapper]._id
                }).exec(function (err, companyInfo) {
                    if (!err) {
                        if (companyInfo != null) {
                            companyInfo.company = {
                                industry: (reqBody[reqWrapper].industry) ? reqBody[reqWrapper].industry : companyInfo.company.industry,
                                revenue: (reqBody[reqWrapper].revenue) ? reqBody[reqWrapper].revenue : companyInfo.company.revenue,
                                employeeSize: (reqBody[reqWrapper].employeeSize) ? reqBody[reqWrapper].employeeSize : companyInfo.company.employeeSize
                            }
                            companyInfo.save(function (errSavingComp, updatedComp) {
                                if (!errSavingComp) {
                                    var uC = {};
                                    if (updatedComp.company) {
                                        uC._id = updatedComp._id;
                                        uC.userId = updatedComp.userId;
                                        uC.industry = updatedComp.company.industry;
                                        uC.revenue = updatedComp.company.revenue;
                                        uC.employeeSize = updatedComp.company.employeeSize;
                                    }
                                    resp[responseUnwrapper] = uC;
                                    resp.success = true;
                                    resp.message = "Successful";
                                    res.json(resp);
                                } else {
                                    resp.error = errSavingComp;
                                    res.json(resp);
                                }
                            })
                        } else {
                            resp.message = "Could not find any matched company";
                            res.json(resp);
                        }
                    } else {
                        resp.message = "Error updating company";
                        res.json(resp);
                    }
                });
            } else {
                new companyInfoModel({
                    userId: reqBody[reqWrapper].userId,
                    company: {
                        industry: (reqBody[reqWrapper].industry) ? reqBody[reqWrapper].industry : "",
                        revenue: (typeof reqBody[reqWrapper].revenue) ? reqBody[reqWrapper].revenue : "",
                        employeeSize: (reqBody[reqWrapper].employeeSize) ? reqBody[reqWrapper].employeeSize : "",
                    }
                }).save(function (err, savedCompany) {
                    if (!err) {
                        var companyDoc = {}
                        if (savedCompany.company) {
                            companyDoc._id = savedCompany._id;
                            companyDoc.userId = savedCompany.userId;
                            companyDoc.industry = savedCompany.company.industry;
                            companyDoc.revenue = savedCompany.company.revenue;
                            companyDoc.employeeSize = savedCompany.company.employeeSize;
                        }
                        resp[responseUnwrapper] = companyDoc;
                        resp.success = true;
                        resp.message = "Successful";
                        res.status(200).json(resp);
                    } else {
                        resp.error = err;
                        res.json(resp);
                    }
                })
            }
        } else {
            res.json(resp);
        }
    });

    // update company Info
    // // I supposed to verify userId before saving it but can't do that now
    router.put("/updateCompanyInfo", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request has no body or userId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "") {
            userInfoModel.findOne({_id: reqBody[reqWrapper].userId}).exec(function(errUser, user){
                if(!errUser && user){
                    user.company.companyName = reqBody[reqWrapper].companyName ? reqBody[reqWrapper].companyName : user.company.companyName ;
                    user.company.companyWebsite = reqBody[reqWrapper].companyWebsite ? reqBody[reqWrapper].companyWebsite : user.company.companyWebsite;
                    user.company.companyLogo = reqBody[reqWrapper].logo ? reqBody[reqWrapper].logo : user.company.companyLogo;
                    user.save(function(ErrUpdatingUser, updatedUser){
                        if(!ErrUpdatingUser){
                            if(reqBody[reqWrapper]._id){
                                companyInfoModel.findOne({ _id: reqBody[reqWrapper]._id }).exec(function (err, companyInfo) {
                                    if (!err) {
                                        
                                        if(companyInfo != null) {
                                            companyInfo.company = {
                                                industry: (reqBody[reqWrapper].industry) ? reqBody[reqWrapper].industry : companyInfo.company.industry,
                                                revenue: (reqBody[reqWrapper].revenue) ? reqBody[reqWrapper].revenue : companyInfo.company.revenue,
                                                employeeSize: (reqBody[reqWrapper].employeeSize) ? reqBody[reqWrapper].employeeSize : companyInfo.company.employeeSize
                                            }
                                            companyInfo.save(function (errSavingComp, updatedComp) {
                                                if (!errSavingComp) {
                                                    console.log(updatedComp);
                                                    var dataToSend = {};
                                                    dataToSend.companyName = reqBody[reqWrapper].companyName;
                                                    dataToSend.companyWebsite = reqBody[reqWrapper].companyWebsite;
                                                    dataToSend.logo = reqBody[reqWrapper].logo
                                                    dataToSend.companyWebsite = reqBody[reqWrapper].revenue;
                                                    dataToSend.companyWebsite = reqBody[reqWrapper].industry;
                                                    dataToSend.companyWebsite = reqBody[reqWrapper].employeeSize;
                                                    resp[responseUnwrapper] = dataToSend;
                                                    resp.success = true;
                                                    resp.message = "Successful";
                                                    res.json(resp);
                                                } else {
                                                    resp.error = errSavingComp;
                                                    res.json(resp);
                                                }
                                            })
                                        } else {
                                            resp.message = "Can't request find company info _id";
                                            res.json(resp);
                                        }
                                    } else {
                                        resp.message = "Error updating company";
                                        res.json(resp);
                                    }
                                });
                            } else {
                                new companyInfoModel({
                                    userId: reqBody[reqWrapper].userId,
                                    company:{
                                        industry: (reqBody[reqWrapper].industry) ? reqBody[reqWrapper].industry : "",
                                        revenue: (reqBody[reqWrapper].revenue) ? reqBody[reqWrapper].revenue : "",
                                        employeeSize: (reqBody[reqWrapper].employeeSize) ? reqBody[reqWrapper].employeeSize : ""
                                    }
                                }).save(function (errSavingComp, updatedComp) {
                                    if (!errSavingComp) {
                                        console.log(updatedComp);
                                        var dataToSend = {};
                                        dataToSend.companyName = reqBody[reqWrapper].companyName;
                                        dataToSend.companyWebsite = reqBody[reqWrapper].companyWebsite;
                                        dataToSend.logo = reqBody[reqWrapper].logo
                                        dataToSend.revenue = reqBody[reqWrapper].revenue;
                                        dataToSend.industry = reqBody[reqWrapper].industry;
                                        dataToSend.employeeSize = reqBody[reqWrapper].employeeSize;
                                        resp[responseUnwrapper] = dataToSend;
                                        resp.success = true;
                                        resp.message = "Successful";
                                        res.json(resp);
                                    } else {
                                        resp.error = errSavingComp;
                                        res.json(resp);
                                    }
                                })
                            }
                        } else {
                            resp.message = "Error finding user";
                            res.json(resp);
                        }
                    })
                } else {
                    resp.message = "Unable to find user";
                    res.json(resp);
                }
            })
        } else {
            res.json(resp);
        }
    });

    // delete company Info
    router.delete("/deleteCompanyInfo", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request has no body or userId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].userId && reqBody[reqWrapper]._id != "" && reqBody[reqWrapper]._id) {
            var query = null;
            if (reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "") {
                query = {
                    userId: reqBody[reqWrapper].userId
                }
            } else if (reqBody[reqWrapper]._id) {
                query = {
                    _id: reqBody[reqWrapper]._id
                }
            } else {
                res.json(resp);
                return;
            }
            companyInfoModel.remove(query, function (err) {
                if (!err) {
                    resp.success = true;
                    resp.message = "Deleted company info successfully";
                    res.status(200).json(resp);
                } else {
                    resp.error = err;
                    resp.message = "Unsuccessful";
                    res.json(resp);
                }
            });
        } else {
            resp.message = "Request has no body or _id";
            res.json(resp);
        }
    });

    // get user Activation Info
    router.get("/getUserActivation", (req, res) => {
        var resp = {
            message: "Unable to fetch user info.",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        var userId = reqQuery["userId"];
        if (reqQuery !== undefined && reqQuery && userId != "") {
            userActivationModel.findOne({ userId: userId }).exec(function (err, userActivation) {
                if (!err) {
                    if (userActivation == null) {
                        resp.success = false;
                        resp.message = "There is no matched document";
                    } else {
                        resp.success = true;
                        resp.message = "Succesful";
                    }
                    resp[responseUnwrapper] = userActivation;
                    res.status(200).json(resp)
                } else {
                    resp.error = err;
                    resp.message = "Error retrieving any user activation document";
                    res.json(resp)
                }
            })
        } else {
            resp.message = "Request has no body or userId";
            res.json(resp);
        }
    });

    // save user Activation Info
    router.post("/saveUserActivation", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Unable to authenticate",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].token && reqBody[reqWrapper].token != "" && reqBody[reqWrapper].passWord && reqBody[reqWrapper].passWord != "") {
            jwt.verify(reqBody[reqWrapper].token, app.get('clientSecret'), function(err, decoded) {
                if (err) {
                    res.json(resp);
                } else {

                    // check if user is already activated
                    userInfoModel.findOne({_id: decoded.userId}).exec(function(errChecking, user){
                        if(!errChecking){
                            if(user.activated){
                                resp[responseUnwrapper] = null;
                                resp.success = false;
                                resp.message = "User already activated!";
                                resp.alreadyActivated = true;
                                res.json(resp);
                            } else {
                                if(reqBody[reqWrapper].userId){
                                    userActivationModel.findOne({userId: reqBody[reqWrapper].userId}).exec(function(errCantFindUserActivation, userActivation){
                                        if(!errCantFindUserActivation){
                                            if(userActivation){
                                                userActivation.passWord = (reqBody[reqWrapper].passWord) ? reqBody[reqWrapper].passWord : userActivation.passWord;
                                                if(userActivation.userSecurityQuestion && userActivation.userSecurityQuestion.question){
                                                    userActivation.userSecurityQuestion.question = (reqBody[reqWrapper].question) ? reqBody[reqWrapper].question : userActivation.userSecurityQuestion.question;
                                                    userActivation.userSecurityQuestion.answer = (reqBody[reqWrapper].answer) ? reqBody[reqWrapper].answer : userActivation.userSecurityQuestion.answer;
                                                } else {
                                                    userActivation.userSecurityQuestion = {};
                                                    userActivation.userSecurityQuestion.question = reqBody[reqWrapper].question ? reqBody[reqWrapper].question : "";
                                                    userActivation.userSecurityQuestion.answer = reqBody[reqWrapper].answer ? reqBody[reqWrapper].answer : "";
                                                }
                                                userActivation.save(function(errUpdatingUserActivation, updateUserActivation){
                                                    if (!errUpdatingUserActivation) {
                                                        resp[responseUnwrapper] = user; //{};//
                                                        resp.success = true;
                                                        resp.message = "Successful";
                                                        res.status(200).json(resp);
                                                    } else {
                                                        resp.error = errUpdatingUserActivation;
                                                        res.json(resp);
                                                    }
                                                })
                                            } else {
                                                resp.message = "User does not has not activated his/her account.";
                                                res.json(resp);
                                            }
                                        } else {
                                            resp.message = "Error while looking to user activation data.";
                                            res.json(resp);
                                        }
                                    })
                                } else {
                                    new userActivationModel({
                                        passWord: (typeof reqBody[reqWrapper].passWord !== undefined) ? reqBody[reqWrapper].passWord : "",
                                        userId: (typeof user.id !== undefined) ? user._id : "",
                                        userSecurityQuestion: {
                                            question: (typeof reqBody[reqWrapper].secQuestion !== undefined) ? reqBody[reqWrapper].secQuestion : "",
                                            answer: (typeof reqBody[reqWrapper].secAnswer !== undefined) ? reqBody[reqWrapper].secAnswer : ""
                                        }
                                    }).save(function (err, userActivation) {
                                        if (!err) {
                                            resp[responseUnwrapper] = user; //{};//
                                            resp.success = true;
                                            resp.message = "Successful";
                                            res.status(200).json(resp);
                                        } else {
                                            resp.error = err;
                                            res.json(resp);
                                        }
                                    })
                                }
                            }
                        }
                    })
                }
            });
        } else{
            res.json(resp);
        }
            // if (reqBody[reqWrapper]._id && reqBody[reqWrapper]._id != "") {
            //     userActivationModel.findOne({
            //         userId: reqBody[reqWrapper].userId,
            //         _id: reqBody[reqWrapper]._id
            //     }).exec(function (err, userActivation) {
            //         if (!err) {
            //             if (userActivation) {
            //                 userActivation = {
            //                     workEmail: (reqBody[reqWrapper].workEmail) ? reqBody[reqWrapper].workEmail : userActivation.workEmail,
            //                     userSecurityQuestion: {
            //                         question: (reqBody[reqWrapper].question) ? reqBody[reqWrapper].question : userActivation.userSecurityQuestion.question,
            //                         answer: (reqBody[reqWrapper].answer) ? reqBody[reqWrapper].answer : userActivation.userSecurityQuestion.question
            //                     }
            //                 };
            //             }
            //             userActivation.save(function (err, updatedUserActivation) {
            //                 if (!err) {
            //                     resp[responseUnwrapper] = updatedUserActivation;
            //                     resp.success = true;
            //                     resp.message = "Successful";
            //                     res.status(200).json(resp);
            //                 } else {
            //                     resp.error = err;
            //                     res.json(resp);
            //                 }
            //             });
            //         } else {
            //             resp.message = "Could not find any matched user activation";
            //             res.json(resp);
            //         }
            //     });
            // } else {
            //     new userActivationModel({
            //         passWord: (typeof reqBody[reqWrapper].passWord !== undefined) ? reqBody[reqWrapper].passWord : "",
            //         userId: (typeof reqBody[reqWrapper].userId !== undefined) ? reqBody[reqWrapper].userId : "",
            //         userSecurityQuestion: {
            //             question: (typeof reqBody[reqWrapper].secQuestion !== undefined) ? reqBody[reqWrapper].secQuestion : "",
            //             answer: (typeof reqBody[reqWrapper].secAnswer !== undefined) ? reqBody[reqWrapper].secAnswer : ""
            //         }
            //     }).save(function (err, userActivation) {
            //         if (!err) {
            //             resp[responseUnwrapper] = userActivation; //{};//
            //             resp.success = true;
            //             resp.message = "Successful";
            //             res.status(200).json(resp);
            //         } else {
            //             resp.error = err;
            //             res.json(resp);
            //         }
            //     })
            // }
    });

    // update user activation Info -- now in saveUserActivation
    router.put("/updateUserActivation", (req, res) => {
        var resp = {
            data: {},
            message: "Unsuccessfull",
            success: false,
            error: null
        }
        var reqBody = req.body;
        if (reqBody && reqBody.userId !== undefined && reqBody.userId != "") {
            userActivationModel.findOne({
                userId: req.body.userId,

            }).exec(function (err, userActivation) {
                if (!err) {
                    userActivation = {
                        workEmail: (typeof reqBody.workEmail !== undefined) ? reqBody.workEmail : userActivation.workEmail,
                        userSecurityQuestion: {
                            question: (typeof reqBody.userSecurityQuestion.question !== undefined) ? reqBody.userSecurityQuestion.question : userActivation.userSecurityQuestion.question,
                            answer: (typeof reqBody.userSecurityQuestion.answer !== undefined) ? reqBody.userSecurityQuestion.answer : userActivation.userSecurityQuestion.question
                        }
                    };
                    userActivation.save(function (err, userActivation) {
                        if (!err) {
                            resp.data = userActivation;
                            resp.success = true;
                            resp.message = "Successful";
                            res.json(resp);
                        } else {
                            resp.error = err;
                            res.json(resp);
                        }
                    });
                } else {
                    resp.message = "Could not find any matched user activation";
                    res.json(resp);
                }
            });
        } else {
            resp.message = "Request has no body or userId";
            res.json(resp);
        }
    });

    // delete user User Activation
    router.delete("/deleteUserActivation", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request has no body or userId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody && eqBody[reqWrapper] && eqBody[reqWrapper]._id) {
            userActivationModel.remove({ _id: reqBody[reqWrapper]._id }, function (err) {
                if (!err) {
                    resp.success = true;
                    resp.message = "Deleted user activation successfully";
                    res.status(200).json(resp);
                }
                else {
                    resp.error = err
                    res.json(resp);
                }
            });
        } else {
            resp.message = "Request has no body or _id";
            res.json(resp);
        }
    });

    // get user user Interest
    router.get("/getUserInterest", (req, res) => {
        var resp = {
            message: "Unable to fetch user info.",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        var userId = reqQuery["userId"];
        if (reqQuery !== undefined && reqQuery && userId != "") {
            userAreaInterestModel.findOne({ userId: userId }).exec(function (err, userInterest) {
                if (!err) {
                    var interest = {};
                    resp[responseUnwrapper] = userInterest;
                    resp.success = true;
                    resp.message = "Succesful";
                    res.status(200).json(resp)
                } else {
                    resp.error = err;
                    resp.message = "Error retrieving any user interest";
                    res.json(resp)
                }
            })
        } else {
            resp.message = "Request has no body or userId";
            res.json(resp);
        }
    });

    // save user Interest
    // I supposed to verify userId before saving it but can't do that now
    router.post("/saveUserInterest", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request body or userId not found in request body",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "") {
            if (reqBody[reqWrapper]._id && reqBody[reqWrapper]._id != "") {
                userAreaInterestModel.findOne({ userId: reqBody[reqWrapper].userId }).exec(function (err, userInterest) {
                    if (!err) {
                        if (userInterest) {
                            userInterest.mainAreaInterest = (reqBody[reqWrapper].mainAreaInterest && Object.keys(reqBody[reqWrapper].mainAreaInterest).length > 0) ? reqBody[reqWrapper].mainAreaInterest : userInterest.areaInterest;
                            userInterest.otherAreaInterest = (reqBody[reqWrapper].otherAreaInterest && Object.keys(reqBody[reqWrapper].otherAreaInterest).length > 0) ? reqBody[reqWrapper].otherAreaInterest : userInterest.otherAreaInterest;
                        }
                        userInterest.save(function (err, updatedUserInterest) {
                            if (!err) {
                                resp[responseUnwrapper] = updatedUserInterest;
                                resp.success = true;
                                resp.message = "Successful";
                                res.status(200).json(resp);
                            } else {
                                resp.error = err;
                                res.json(resp);
                            }
                        });
                    } else {
                        resp.message = "Could not find any matched User Interest";
                        res.json(resp);
                    }
                });
            } else {
                new userAreaInterestModel({
                    userId: reqBody[reqWrapper].userId,
                    mainAreaInterest: (reqBody[reqWrapper].mainAreaInterest && Object.keys(reqBody[reqWrapper].mainAreaInterest).length > 0) ? reqBody[reqWrapper].mainAreaInterest : {},
                    otherAreaInterest: (reqBody[reqWrapper].otherAreaInterest && Object.keys(reqBody[reqWrapper].otherAreaInterest).length > 0) ? reqBody[reqWrapper].otherAreaInterest : {}
                }).save(function (err, userInterest) {
                    if (!err) {
                        resp[responseUnwrapper] = userInterest;
                        resp.success = true;
                        resp.message = "Successful";
                        res.status(200).json(resp);
                    } else {
                        resp.error = err;
                        res.json(resp);
                    }
                })
            }

        } else {
            res.json(resp);
        }
    });

    // update user interest 
    router.put("/updateUserInterest", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Unable to sign up",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "" && reqBody[reqWrapper]._id) {
            userAreaInterestModel.findOne({ userId: reqBody[reqWrapper].userId }).exec(function (err, userInterest) {
                if (!err) {
                    if (userInterest) {
                        userInterest.areaInterest = (reqBody[reqWrapper].mainAreaInterest && Object.keys(reqBody[reqWrapper].mainAreaInterest).length > 0) ? reqBody[reqWrapper].mainAreaInterest : userInterest.areaInterest;
                        userInterest.moreInterest = (reqBody[reqWrapper].otherAreaInterest && Object.keys(reqBody[reqWrapper].otherAreaInterest).length > 0) ? reqBody[reqWrapper].otherAreaInterest : userInterest.otherAreaInterest;
                    }
                    userActivation = {
                        workEmail: (typeof reqBody.workEmail !== undefined) ? reqBody.workEmail : userActivation.workEmail,
                        userSecurityQuestion: {
                            question: (typeof reqBody.userSecurityQuestion.question !== undefined) ? reqBody.userSecurityQuestion.question : userActivation.userSecurityQuestion.question,
                            answer: (typeof reqBody.userSecurityQuestion.answer !== undefined) ? reqBody.userSecurityQuestion.answer : userActivation.userSecurityQuestion.question
                        }
                    };
                    userActivation.save(function (err, userActivation) {
                        if (!err) {
                            resp.data = userActivation;
                            resp.success = true;
                            resp.message = "Successful";
                            res.json(resp);
                        } else {
                            resp.error = err;
                            res.json(resp);
                        }
                    });
                } else {
                    resp.message = "Could not find any matched USer Interest";
                    res.json(resp);
                }
            });
        } else {
            resp.message = "Request has no body";
            res.json(resp);
        }
    });

    // delete user Interest
    router.delete("/deleteUserInterest", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request has no body or userId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody && reqBody[reqWrapper] && reqBody[reqWrapper]._id) {
            userActivationModel.remove({ _id: reqBody[reqWrapper]._id }, function (err) {
                if (!err) {
                    resp.success = true;
                    resp.message = "Deleted user activation successfully";
                    res.json(resp);
                }
                else {
                    resp.error = err
                    res.json(resp);
                }
            });
        } else {
            resp.message = "Request has no body or _id";
            res.json(resp);
        }
    });

    // get user user professional info.
    router.get("/getUserProfessionalInfo", (req, res) => {
        var resp = {
            message: "Unable to fetch user info.",
            success: false,
            error: null
        }
        var reqQuery = req.query;
        let responseUnwrapper = (reqQuery.unwrapper) ? reqQuery.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        var userId = reqQuery["userId"];
        if (reqQuery !== undefined && reqQuery && userId != "") {
            userProfessionalInfoModel.findOne({ userId: userId }).exec(function (err, userProfInfo) {
                if (!err) {
                    if (userProfInfo == null) {
                        resp.success = false;
                        resp.message = "There is no matched document";
                    } else {
                        resp.success = true;
                        resp.message = "Succesful";
                    }
                    resp[responseUnwrapper] = userProfInfo;
                    res.status(200).json(resp)
                } else {
                    resp.error = err;
                    resp.message = "Error retrieving any user professional info.";
                    res.json(resp)
                }
            })
        } else {
            resp.message = "Request has no body or userId";
            res.json(resp);
        }
    });

    // save user Professional Info
    // I supposed to verify userId before saving it but can't do that now
    router.post("/saveUserProfessionalInfo", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request body or userId not found in request body",
            success: false,
            error: null
        }
        console.log(req.body);
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "") {
            if (reqBody[reqWrapper].personalInfo && reqBody[reqWrapper].personalInfo._id != "") {
                userProfessionalInfoModel.findOne({
                    userId: reqBody[reqWrapper].userId,
                    _id: reqBody[reqWrapper].personalInfo._id
                }).exec(function (errGettingUserProfInfo, userProfInfo) {
                    if (!errGettingUserProfInfo) {
                        if(reqBody[reqWrapper].personalInfo ){
                            userProfInfo.currentWorkExperience = (reqBody[reqWrapper].personalInfo && reqBody[reqWrapper].personalInfo.currentWorkExperience && Object.keys(reqBody[reqWrapper].personalInfo.currentWorkExperience).length > 0) ? reqBody[reqWrapper].personalInfo.currentWorkExperience : userProfInfo.currentWorkExperience;
                        }
                        if(reqBody[reqWrapper].personalInfo && reqBody[reqWrapper].personalInfo.currentWorkExp && reqBody[reqWrapper].currentWorkExp.previousWorkExperiances){
                            userProfInfo.previousWorkExperiances = (reqBody[reqWrapper].personalInfo && reqBody[reqWrapper].personalInfo.previousWorkExperiances && reqBody[reqWrapper].personalInfo.previousWorkExperiances instanceof Array) ? reqBody[reqWrapper].personalInfo.previousWorkExperiances : userProfInfo.previousWorkExperiances;
                        }
                        userProfInfo.save(function (err, updatedUserProfInfo) {
                            if (!err) {
                                // update user job title in the user info collection
                                if(reqBody[reqWrapper].jobTitle){
                                    userInfoModel.findOne({_id: reqBody[reqWrapper].userId}).exec(function(userErr, userFound){
                                        if(!userErr){
                                            if(userFound && userFound.job && userFound.job.jobTitle){
                                                userFound.job.jobTitle = (reqBody[reqWrapper].jobTitle ) ? reqBody[reqWrapper].jobTitle : userFound.job.jobTitle;
                                                userFound.company.companyName = (reqBody[reqWrapper].companyName) ? reqBody[reqWrapper].companyName : userFound.company.companyName;
                                                userFound.company.companyWebsite = (reqBody[reqWrapper].companyWebsite) ? reqBody[reqWrapper].companyWebsite : userFound.company.companyWebsite;
                                                userFound.company.companyLogo = (reqBody[reqWrapper].logo) ? reqBody[reqWrapper].logo : userFound.company.companyLogo;
                                                userFound.save(function(errUserJobTitleUpdate, userJobTitleUpdated){
                                                    if(!errUserJobTitleUpdate){
                                                        new MongoClient(mongodbConn, {
                                                            auth: {
                                                                user: azureCosmoDBUser,
                                                                password: azureCosmoDBPassword
                                                            }
                                                        }).connect((err, db) => {
                                                            if (err) {
                                                                var dataToSend = {}
                                                                dataToSend.personalInfo = updatedUserProfInfo;
                                                                dataToSend.companyName = userJobTitleUpdated.company.companyName;
                                                                dataToSend.jobTitle = userJobTitleUpdated.job.jobTitle;
                                                                resp[responseUnwrapper] = dataToSend;
                                                                resp.success = true;
                                                                resp.message = "Successful";
                                                                res.json(resp);
                                                                
                                                            } else {
                                                                var dbo = db.db(databaseName);
                                                                dbo.collection("companies").findOne({
                                                                    "companyName": { $regex: new RegExp(reqBody[reqWrapper].company.toLowerCase(), "i") }, 
                                                                    "companyWebsite": { $regex: new RegExp(reqBody[reqWrapper].companySite.toLowerCase(), "i") } },
                                                                    function (err, result) {
                                                                    if (!err) {
                                                                        if(result){
                                                                            new companyInfoModel({
                                                                                userId: userFound._id,
                                                                                company: {
                                                                                    industry: (result.companyPrimaryIndustry) ? result.companyPrimaryIndustry : "",
                                                                                    // revenue: (result.companyRevenue) ? result.companyRevenue : "",
                                                                                    // employeeSize: (result.numberOfEmployees) ? result.numberOfEmployees : ""
                                                                                    revenue: (result.companyRevenueRange) ? result.companyRevenueRange : "",
                                                                                    employeeSize: (result.employeeRange) ? result.employeeRange : ""
                                                                                }
                                                                            }).save(function(errSavingUserCompanyInfo, savingUserCompanyInfo){
                                                                                if(!errSavingUserCompanyInfo){
                                                                                    if(savingUserCompanyInfo){
                                                                                        var dataToSend = {}
                                                                                        dataToSend.personalInfo = updatedUserProfInfo;
                                                                                        dataToSend.companyName = userJobTitleUpdated.company.companyName;
                                                                                        dataToSend.jobTitle = userJobTitleUpdated.job.jobTitle;
                                                                                        resp[responseUnwrapper] = dataToSend;
                                                                                        resp.success = true;
                                                                                        resp.message = "Successful";
                                                                                        res.json(resp);
                                                                                    } else {
                                                                                        var dataToSend = {}
                                                                                        dataToSend.personalInfo = updatedUserProfInfo;
                                                                                        dataToSend.companyName = userJobTitleUpdated.company.companyName;
                                                                                        dataToSend.jobTitle = userJobTitleUpdated.job.jobTitle;
                                                                                        resp[responseUnwrapper] = dataToSend;
                                                                                        resp.success = true;
                                                                                        resp.message = "Successful";
                                                                                        res.json(resp);
                                                                                    }
                                                                                    
                                                                                    // console.log(savingUserCompanyInfo);
                                                                                } else {
                                                                                    var dataToSend = {}
                                                                                    dataToSend.personalInfo = updatedUserProfInfo;
                                                                                    dataToSend.companyName = userJobTitleUpdated.company.companyName;
                                                                                    dataToSend.jobTitle = userJobTitleUpdated.job.jobTitle;
                                                                                    resp[responseUnwrapper] = dataToSend;
                                                                                    resp.success = true;
                                                                                    resp.message = "Successful";
                                                                                    res.json(resp);
                                                                                }
                                                                            })
                                                                        }
                                                                    } else {
                                                                        // "Error saving user company info"
                                                                        var dataToSend = {}
                                                                        dataToSend.personalInfo = updatedUserProfInfo;
                                                                        dataToSend.companyName = userJobTitleUpdated.company.companyName;
                                                                        dataToSend.jobTitle = userJobTitleUpdated.job.jobTitle;
                                                                        resp[responseUnwrapper] = dataToSend;
                                                                        resp.success = true;
                                                                        resp.message = "Successful";
                                                                        res.json(resp);
                                                                    }
                                                                    db.close();
                                                                });
                                                            }
                                                        })
                                                        
                                                    } else {
                                                        resp[responseUnwrapper] = updatedUserProfInfo;
                                                        resp.success = true;
                                                        resp.message = "Successful update current work experience but couldn't update job title";
                                                        res.json(resp);
                                                    }
                                                })
                                            } else {
                                                resp[responseUnwrapper] = null;
                                                resp.success = true;
                                                resp.message = "There is no user with this id";
                                                res.json(resp);
                                            }
                                        } else {
                                            resp[responseUnwrapper] = updatedUserProfInfo;
                                            resp.success = true;
                                            resp.message = "There is no user with this id";
                                            res.json(resp);
                                        }
                                    })
                                } else {
                                    resp[responseUnwrapper] = updatedUserProfInfo;
                                    resp.success = true;
                                    resp.message = "Successful update current work experience but couldn't update job title";
                                    res.json(resp);
                                }
                            } else {
                                resp.error = err;
                                res.json(resp);
                            }
                        });
                    } else {
                        resp.message = "Could not find any matched User Professional Info";
                        res.json(resp);
                    }
                });
            } else {
                new userProfessionalInfoModel({
                    userId: reqBody[reqWrapper].userId,
                    currentWorkExperience: reqBody[reqWrapper].currentWorkExperience,
                    previousWorkExperiances: (reqBody[reqWrapper].previousWorkExperiances && reqBody[reqWrapper].previousWorkExperiances instanceof Array) ? reqBody[reqWrapper].previousWorkExperiances : []
                }).save(function (err, userInterest) {
                    if (!err) {
                        resp[responseUnwrapper] = userInterest;
                        resp.success = true;
                        resp.message = "Successful";
                        res.json(resp);
                    } else {
                        resp.error = err;
                        res.json(resp);
                    }
                })
            }
        } else {
            res.json(resp);
        }
    });

    // update user Professional Info
    // Can't work on this now
    router.put("/updateUserProfessionalInfo", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request body or userId not found in request body",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody[reqWrapper] && reqBody[reqWrapper].userId && reqBody[reqWrapper].userId != "" && reqBody[reqWrapper]._id && reqBody[reqWrapper]._id != "") {
            userProfessionalInfoModel.findOne({
                userId: reqBody[reqWrapper].userId,
                _id: reqBody[reqWrapper]._id
            }).exec(function (errGettingUserProfInfo, userProfInfo) {
                if (!errGettingUserProfInfo) {
                    userProfInfo.currentWorkExperience = (reqBody[reqWrapper].currentWorkExperience && Object.keys(reqBody[reqWrapper].currentWorkExperience).length > 0) ? reqBody[reqWrapper].currentWorkExperience : userProfInfo.currentWorkExperience;
                    userProfInfo.previousWorkExperiances = (reqBody[reqWrapper].previousWorkExperiances && reqBody[reqWrapper].previousWorkExperiances instanceof Array && reqBody[reqWrapper].previousWorkExperiances.length > 0) ? reqBody[reqWrapper].previousWorkExperiances : userProfInfo.previousWorkExperiances;
                    userProfInfo.save(function (err, updatedUserProfInfo) {
                        if (!err) {
                            resp[responseUnwrapper] = updatedUserProfInfo;
                            resp.success = true;
                            resp.message = "Successful";
                            res.json(resp);
                        } else {
                            resp.error = err;
                            res.json(resp);
                        }
                    });
                } else {
                    resp.message = "Could not find any matched User Professional Info";
                    res.json(resp);
                }
            });
        } else {
            resp.message = "Request has no body";
            res.json(resp);
        }
    });

    // delete user Professional Info
    router.delete("/deleteUserProfessionalInfo", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            message: "Request has no body or userId",
            success: false,
            error: null
        }
        var reqBody = req.body;
        let reqWrapper = reqBody.wrapper;
        let responseUnwrapper = (reqBody.unwrapper) ? reqBody.unwrapper : 'data';
        resp[responseUnwrapper] = null;
        if (reqBody && reqBody[reqWrapper] && reqBody[reqWrapper]._id && reqBody[reqWrapper]._id != "") {
            userAreaInterestModel.remove({ _id: reqBody[reqWrapper]._id }, function (err) {
                if (!err) {
                    resp.success = true;
                    resp.message = "Deleted user activation successfully";
                    res.json(resp);
                }
                else {
                    resp.error = err
                    res.json(resp);
                }
            });
        } else {
            resp.message = "Request has no body or _id";
            res.json(resp);
        }
    });

    // i need to see what will be sent to work on this
    router.post("/saveUserProfilePicture", (req, res) => {
        var resp = {
            data: {},
            message: "Unsuccessfull",
            success: false,
            error: null
        }
        var reqBody = req.body;
        res.send("Received");
    })

    // authenticate user
    router.post('/authenticate', function (req, res) {
        // find the user
        var user = {
            firstName: "Ayo",
            lastName: "Ajayi"
        }
        if (req.body) {
            var token = jwt.sign(user, app.get('clientSecret'), {
                expiresIn: 60 * 60 * 24 // expires in 24 hours
            });

            // return the information including token as JSON
            res.json({
                success: true,
                message: 'Enjoy your token!',
                token: token
            });
        }
        // User.findOne({
        //     name: req.body.name
        // }, function(err, user) {
        //     if (err) throw err;
        //     if (!user) {
        //         res.json({
        //             success: false,
        //             message: 'Authentication failed. User not found.'
        //         });
        //     } else if (user) {
        //         // check if password matches
        //         if (!user.validPassword(req.body.password)) {
        //             res.json({
        //                 success: false,
        //                 message: 'Authentication failed. Wrong password.'
        //             });
        //         } else {
        //             // if user is found and password is right
        //             // create a token
        //             var token = jwt.sign(user, app.get('clientSecret'), {
        //                 expiresIn: 60 * 60 * 24 // expires in 24 hours
        //             });

        //             // return the information including token as JSON
        //             res.json({
        //                 success: true,
        //                 message: 'Enjoy your token!',
        //                 token: token
        //             });
        //         }
        //     }
        // });
    });

    // save into app config object
    // this is probably not gonna be used 
    router.post("/saveAppConfig", (req, res) => {
        // resp is the object to send back to UI
        var resp = {
            data: {},
            message: "Unsuccessfull",
            success: false,
            error: null
        }
        appConfigModel.find().exec(function(err, appconfig){
            if(!err){
                if(appconfig && appconfig instanceof Array && appconfig.length >0){
                    let newAppConfig = appconfig[0];
                    newAppConfig.industries = ["Accounting Services", "Advertising/Marketing", "Agriculture Forestry", "Architecture Engineering", "Automotive", "Banking", "Beverages", "Broadcast Media", "Casinos Gambling", "Chemicals", "Computer Software", "Construction", "Consumer Products", "Defense Aerospace", "Department Stores", "e Commerce", "Energy/Utilities", "Federal", "Financial Services", "Food Processing", "Gaming Software/Systems", "Health Insurance", "Higher Education", "Holding Companies", "Hospitals/Health Care", "Insurance", "IT Services", "Legal Services", "Leisure", "Local", "Lodging", "Logistics/Transportation", "Manufacturing Durables", "Manufacturing Non-Durables", "Medical Devices", "Minerals Mining", "Non-Profit", "Oil & Gas", "Pharmaceuticals", "Print & Digital Media", "Professional Services", "Professional Sports", "Real Estate/Property Management", "Religious Organizations", "Restaurants", "Retail", "School Districts", "Services", "Social Media", "Staffing Recruiting", "State", "Supermarkets", "Telecom/Communication Services", "Travel & Tourism", "Venture Capital Private Equity", "Waste Management", "Wholesale"]
                    newAppConfig.numberOfEmployees = ["1-10", "11-50", "51-100", "101-250", "251-500", "501-1,000", "1,001-5,000", "5,001-10,000", "10,001+"];
                    newAppConfig.companyRevenue = ["Less than $1M", "$1M-$10M", "$11M-$25M", "$26M-$50M", "$51M-$100M", "$101M-$250M", "$251M-$500M", "$501M-$1B", "$1B-$3B", "$3B-$5B", "$5B+", ""];
        
                    newAppConfig.jobFunctionList= ["Supply Chain","Procurement","Manufacturing/Ops","Information Technology","Finance","Human Resources","Marketing"];
                    newAppConfig.securityQuestions={
                        questions:[]
                    };
                    newAppConfig.categories= {
                        'Leadership': ['Leadership & Management', 'Training & Development', 'Strategy & Innovation', 'Governance, Risk Management & Compliance', 'Succession Planning', 'Mergers & Acquisitions', 'Funding & Capital'],
                        'Supply Chain': ['Warehousing & Distribution Center Optimization', 'Transportation Management', '3PLS & Logistics', 'Procurement & Strategic Sourcing', 'Supply Chain Planning & Execution', 'Supply Chain Strategy &Leadership', 'Product Development & Innovation', 'Market Disruptors'],
                        'Procurement': ['Supplier Partnership Management', 'Procurement Leadership Strategies', 'Strategic Sourcing', 'Supply Chain Management', 'Contract Negotiation', 'Digitalization of Procurement', 'Market Disruptors'],
                        'Manufacturing/Ops':['Continuous Improvement / Lean','Demand and Production Planning','Digital Manufacturing Transformation','Internet of Things (IoT)','Manufacturing Ops Leadership','Plant & Facilities Management','Product Development & Innovation','Supply Chain Management','Workforce Management'],	
                        'Information Technology': ['Cybersecurity', 'Cloud Strategy', 'Data Management', 'Network Systems', 'Communication Systems', 'Storage and Applications Development', 'General Business Leadership & Innovation'],
                        'Finance': ['Auditing, Accounting and Tax', 'Strategy, Forecasting and Reporting', 'Banking and Capital Markets', 'Governance, Risk and Compliance', 'Enterprise Resource Planning (ERP)', 'General Business Leadership & Innovation'],
                        'Human Resources': ['Recruiting and Staffing', 'Benefits and Compensation', 'Learning, Training and Development', 'Human Resource Information Systems(H.R.I.S.)', 'Employee Engagement', 'Compliance and Employment Law', 'General Business Leadership & Innovation'],
                        'Marketing':['Advertising & Promotion','Data & Analytics','Demand Generation','Digital Marketing & Content Strategy','Event Marketing','Marketing Strategy & Leadership','Marketing Technology & Innovation','Multichannel & Brand Strategy','Sales Enablement','Social & Influencer Marketing']
                    };
                    newAppConfig.bannedDomain=[ "gmail.com", "yahoo.com", "hotmail.com", "aol.com", "hotmail.co.uk", "hotmail.fr", "msn.com", "yahoo.fr", "wanadoo.fr", "orange.fr", "comcast.net", "yahoo.co.uk", "yahoo.com.br", "yahoo.co.in", "live.com", "rediffmail.com", "free.fr", "gmx.de", "web.de", "yandex.ru", "ymail.com", "libero.it", "outlook.com", "uol.com.br", "bol.com.br", "mail.ru", "cox.net", "hotmail.it", "sbcglobal.net", "sfr.fr", "live.fr", "verizon.net", "live.co.uk", "googlemail.com", "yahoo.es", "ig.com.br", "live.nl", "bigpond.com", "terra.com.br", "yahoo.it", "neuf.fr", "yahoo.de", "alice.it", "rocketmail.com", "att.net", "laposte.net", "facebook.com", "bellsouth.net", "yahoo.in", "hotmail.es", "charter.net", "yahoo.ca", "yahoo.com.au", "rambler.ru", "hotmail.de", "tiscali.it", "shaw.ca", "yahoo.co.jp", "sky.com", "earthlink.net", "optonline.net", "freenet.de", "t-online.de", "aliceadsl.fr", "virgilio.it", "home.nl", "qq.com", "telenet.be", "me.com", "yahoo.com.ar", "tiscali.co.uk", "yahoo.com.mx", "voila.fr", "gmx.net", "mail.com", "planet.nl", "tin.it", "live.it", "ntlworld.com", "arcor.de", "yahoo.co.id", "frontiernet.net", "hetnet.nl", "live.com.au", "yahoo.com.sg", "zonnet.nl", "club-internet.fr", "juno.com", "optusnet.com.au", "blueyonder.co.uk", "bluewin.ch", "skynet.be", "sympatico.ca", "windstream.net", "mac.com", "centurytel.net", "chello.nl", "live.ca", "aim.com", "bigpond.net.au" ];
                    newAppConfig.industries =["Accounting Services", "Advertising/Marketing", "Agriculture Forestry", "Architecture Engineering", "Automotive", "Banking", "Beverages", "Broadcast Media", "Casinos Gambling", "Chemicals", "Computer Software", "Construction", "Consumer Products", "Defense Aerospace", "Department Stores", "e Commerce", "Energy/Utilities", "Federal", "Financial Services", "Food Processing", "Gaming Software/Systems", "Health Insurance", "Higher Education", "Holding Companies", "Hospitals/Health Care", "Insurance", "IT Services", "Legal Services", "Leisure", "Local", "Lodging", "Logistics/Transportation", "Manufacturing Durables", "Manufacturing Non-Durables", "Medical Devices", "Minerals Mining", "Non-Profit", "Oil & Gas", "Pharmaceuticals", "Print & Digital Media", "Professional Services", "Professional Sports", "Real Estate/Property Management", "Religious Organizations", "Restaurants", "Retail", "School Districts", "Services", "Social Media", "Staffing Recruiting", "State", "Supermarkets", "Telecom/Communication Services", "Travel & Tourism", "Venture Capital Private Equity", "Waste Management", "Wholesale"];
                    newAppConfig.numberOfEmployees = ["1-10", "11-50", "51-100", "101-250", "251-500", "501-1,000", "1,001-5,000", "5,001-10,000", "10,001+"],
                    newAppConfig.companyRevenue = ["Less than $1M", "$1M-$10M", "$11M-$25M", "$26M-$50M", "$51M-$100M", "$101M-$250M", "$251M-$500M", "$501M-$1B", "$1B-$3B", "$3B-$5B", "$5B+", ""]
                    newAppConfig.save(function(err, appConfigDoc){
                        if(!err){
                            resp.data = appConfigDoc;
                            resp.message = "Successful";
                            resp.success = true;
                            res.json(resp);
                        } else {
                            resp.error = err;
                            res.json(resp);
                        }
                    })
                } else {
                    // var industries = ["Accounting Services", "Advertising/Marketing", "Agriculture Forestry", "Architecture Engineering", "Automotive", "Banking", "Beverages", "Broadcast Media", "Casinos Gambling", "Chemicals", "Computer Software", "Construction", "Consumer Products", "Defense Aerospace", "Department Stores", "e Commerce", "Energy/Utilities", "Federal", "Financial Services", "Food Processing", "Gaming Software/Systems", "Health Insurance", "Higher Education", "Holding Companies", "Hospitals/Health Care", "Insurance", "IT Services", "Legal Services", "Leisure", "Local", "Lodging", "Logistics/Transportation", "Manufacturing Durables", "Manufacturing Non-Durables", "Medical Devices", "Minerals Mining", "Non-Profit", "Oil & Gas", "Pharmaceuticals", "Print & Digital Media", "Professional Services", "Professional Sports", "Real Estate/Property Management", "Religious Organizations", "Restaurants", "Retail", "School Districts", "Services", "Social Media", "Staffing Recruiting", "State", "Supermarkets", "Telecom/Communication Services", "Travel & Tourism", "Venture Capital Private Equity", "Waste Management", "Wholesale"]
                    // var numberOfEmployees = ["1-10", "11-50", "51-100", "101-250", "251-500", "501-1,000", "1,001-5,000", "5,001-10,000", "10,001+"];
                    // var companyRevenue = ["Less than $1M", "$1M-$10M", "$11M-$25M", "$26M-$50M", "$51M-$100M", "$101M-$250M", "$251M-$500M", "$501M-$1B", "$1B-$3B", "$3B-$5B", "$5B+", ""];
                    new appConfigModel({
                        jobFunctionList: ["Supply Chain","Procurement","Manufacturing/Ops","Information Technology","Finance","Human Resources","Marketing"],
                        securityQuestions:{
                            questions:[]
                        },
                        categories: {
                            'Leadership': ['Leadership & Management', 'Training & Development', 'Strategy & Innovation', 'Governance, Risk Management & Compliance', 'Succession Planning', 'Mergers & Acquisitions', 'Funding & Capital'],
                            'Supply Chain': ['Warehousing & Distribution Center Optimization', 'Transportation Management', '3PLS & Logistics', 'Procurement & Strategic Sourcing', 'Supply Chain Planning & Execution', 'Supply Chain Strategy &Leadership', 'Product Development & Innovation', 'Market Disruptors'],
                            'Procurement': ['Supplier Partnership Management', 'Procurement Leadership Strategies', 'Strategic Sourcing', 'Supply Chain Management', 'Contract Negotiation', 'Digitalization of Procurement', 'Market Disruptors'],
                            'Manufacturing/Ops':['Continuous Improvement / Lean','Demand and Production Planning','Digital Manufacturing Transformation','Internet of Things (IoT)','Manufacturing Ops Leadership','Plant & Facilities Management','Product Development & Innovation','Supply Chain Management','Workforce Management'],	
                            'Information Technology': ['Cybersecurity', 'Cloud Strategy', 'Data Management', 'Network Systems', 'Communication Systems', 'Storage and Applications Development', 'General Business Leadership & Innovation'],
                            'Finance': ['Auditing, Accounting and Tax', 'Strategy, Forecasting and Reporting', 'Banking and Capital Markets', 'Governance, Risk and Compliance', 'Enterprise Resource Planning (ERP)', 'General Business Leadership & Innovation'],
                            'Human Resources': ['Recruiting and Staffing', 'Benefits and Compensation', 'Learning, Training and Development', 'Human Resource Information Systems(H.R.I.S.)', 'Employee Engagement', 'Compliance and Employment Law', 'General Business Leadership & Innovation'],
                            'Marketing':['Advertising & Promotion','Data & Analytics','Demand Generation','Digital Marketing & Content Strategy','Event Marketing','Marketing Strategy & Leadership','Marketing Technology & Innovation','Multichannel & Brand Strategy','Sales Enablement','Social & Influencer Marketing']
                        },
                        bannedDomain:[ "gmail.com", "yahoo.com", "hotmail.com", "aol.com", "hotmail.co.uk", "hotmail.fr", "msn.com", "yahoo.fr", "wanadoo.fr", "orange.fr", "comcast.net", "yahoo.co.uk", "yahoo.com.br", "yahoo.co.in", "live.com", "rediffmail.com", "free.fr", "gmx.de", "web.de", "yandex.ru", "ymail.com", "libero.it", "outlook.com", "uol.com.br", "bol.com.br", "mail.ru", "cox.net", "hotmail.it", "sbcglobal.net", "sfr.fr", "live.fr", "verizon.net", "live.co.uk", "googlemail.com", "yahoo.es", "ig.com.br", "live.nl", "bigpond.com", "terra.com.br", "yahoo.it", "neuf.fr", "yahoo.de", "alice.it", "rocketmail.com", "att.net", "laposte.net", "facebook.com", "bellsouth.net", "yahoo.in", "hotmail.es", "charter.net", "yahoo.ca", "yahoo.com.au", "rambler.ru", "hotmail.de", "tiscali.it", "shaw.ca", "yahoo.co.jp", "sky.com", "earthlink.net", "optonline.net", "freenet.de", "t-online.de", "aliceadsl.fr", "virgilio.it", "home.nl", "qq.com", "telenet.be", "me.com", "yahoo.com.ar", "tiscali.co.uk", "yahoo.com.mx", "voila.fr", "gmx.net", "mail.com", "planet.nl", "tin.it", "live.it", "ntlworld.com", "arcor.de", "yahoo.co.id", "frontiernet.net", "hetnet.nl", "live.com.au", "yahoo.com.sg", "zonnet.nl", "club-internet.fr", "juno.com", "optusnet.com.au", "blueyonder.co.uk", "bluewin.ch", "skynet.be", "sympatico.ca", "windstream.net", "mac.com", "centurytel.net", "chello.nl", "live.ca", "aim.com", "bigpond.net.au" ],
                        industries :["Accounting Services", "Advertising/Marketing", "Agriculture Forestry", "Architecture Engineering", "Automotive", "Banking", "Beverages", "Broadcast Media", "Casinos Gambling", "Chemicals", "Computer Software", "Construction", "Consumer Products", "Defense Aerospace", "Department Stores", "e Commerce", "Energy/Utilities", "Federal", "Financial Services", "Food Processing", "Gaming Software/Systems", "Health Insurance", "Higher Education", "Holding Companies", "Hospitals/Health Care", "Insurance", "IT Services", "Legal Services", "Leisure", "Local", "Lodging", "Logistics/Transportation", "Manufacturing Durables", "Manufacturing Non-Durables", "Medical Devices", "Minerals Mining", "Non-Profit", "Oil & Gas", "Pharmaceuticals", "Print & Digital Media", "Professional Services", "Professional Sports", "Real Estate/Property Management", "Religious Organizations", "Restaurants", "Retail", "School Districts", "Services", "Social Media", "Staffing Recruiting", "State", "Supermarkets", "Telecom/Communication Services", "Travel & Tourism", "Venture Capital Private Equity", "Waste Management", "Wholesale"],
                        numberOfEmployees : ["1-10", "11-50", "51-100", "101-250", "251-500", "501-1,000", "1,001-5,000", "5,001-10,000", "10,001+"],
                        companyRevenue : ["Less than $1M", "$1M-$10M", "$11M-$25M", "$26M-$50M", "$51M-$100M", "$101M-$250M", "$251M-$500M", "$501M-$1B", "$1B-$3B", "$3B-$5B", "$5B+", ""],
                        termsAndConditions: "",
                        faqs: "",
                        policy: ""
                    }).save(function(err, appConfigDoc){
                        if(!err){
                            resp.data = appConfigDoc;
                            resp.message = "Successful";
                            resp.success = true;
                            res.json(resp);
                        } else {
                            resp.error = err;
                            res.json(resp);
                        }
                    })
                }
            }
        })
        // res.json(resp)

    });

    //Use our router configuration when we call /api
    app.use('/quartzservice', router);
}

function clearBitSearch(){
    // get user company information
    ClearbitCompanySearch.find({domain: reqBody[reqWrapper].companySite})
    .then(function (company) {
        var userCompanyWebsite = reqBody[reqWrapper].companySite;
        var userCompanyName = reqBody[reqWrapper].company;
        var companyName = company.name;
        var companyDomain = company.domain;
        if(company && companyName && companyName.trim() != "" && companyDomain && companyDomain.trim() != ""){
            if(userCompanyWebsite.toLowerCase() == companyDomain.toLowerCase() && userCompanyName.toLowerCase() == companyName.toLowerCase()){
                doc.company.companyLogo = company.logo;
                doc.save(function(errUpdataUserCompanyLogo, updataUserCompanyLogo){
                    if(!errUpdataUserCompanyLogo){
                        new companyInfoModel({
                            userId: doc._id,
                            company: {
                                industry: (company.category && company.category.industry) ? company.category.industry : "",
                                revenue: (company.metrics && company.metrics.annualRevenue) ? company.metrics.annualRevenue : "",
                                employeeSize: (company.metrics && company.metrics.employeesRange) ? company.metrics.employeesRange : ""
                            }
                        }).save(function(errSavingUserCompanyInfo, savingUserCompanyInfo){
                            if(!errSavingUserCompanyInfo){
                                // userCompany saved;
                                var user = {};
                                user.workEmail = doc.workEmail;
                                user.userId = doc._id;
                                user.name = doc.userFullName.firstName + " " + doc.userFullName.lastName;
                                // sendMail(user);
                            }
                        })
                    }
                });
            } 
        }
    })
    .catch(ClearbitCompanySearch.QueuedError, function (err) {
        console.log("Error saving company information");
    })
    .catch(ClearbitCompanySearch.NotFoundError, function (err) {
        console.log("Error saving company information");
    })
    .catch(function (err) {
        console.log("Error saving company information");
    });
}