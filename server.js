var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var config = require('./config');
//and create our instances
var app = express();
//set our port to either a predetermined port number if you have set 
//it up, or 3001
var apiPort = process.env.PORT || 1337;
var router = express.Router();
const cors = require('cors');
// app.use(cors({origin: 'http://devapp.infodat.com:3030'}));
app.use(cors({origin:true,credentials: true}));
app.set('clientSecret',config.clientSecret);
require('./router')(app,router,bodyParser);

//starts the server and listens for requests
app.listen(apiPort, function() {
 console.log(`services running on port ${apiPort}`);
});

// var server = http.createServer(app);


// server.listen(apiPort, function () {
//     app.setHost(undefined); // change five
// });
/////